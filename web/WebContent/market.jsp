<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>배온-직거래장터</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

<style>
.btn-group { width:100%; }
.btn-primary { width:20%; }
</style>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53425022-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body style="max-width:500px">
<div class="container" style="width:100%">
<div id="menu">	
<%@ include file="menu.jsp" %> 
</div>
<h4>아이템</h4>
   <div class="input-group">
     <input type="text" class="form-control" name="itemname" placeholder="아이템명을 입력해라묘">
     <span class="input-group-btn">
       <button class="btn btn-default" type="button" data-toggle="modal" data-target=".bs-example-modal-sm">목록에서 검색</button>
     </span>
     <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="modal">
  	 <div class="modal-dialog modal-sm">
	    <div class="modal-content">
		    <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
	          <h4 class="modal-title" id="mySmallModalLabel">아이템을 선택해라묘</h4>
	        </div>
		    <div class="modal-body">
	          <ul class="list-group">
				  <li class="list-group-item">얼어붙은 독침</li>
				  <li class="list-group-item">해무진의 혼</li>
				  <li class="list-group-item">봉인된 나류석판</li>
				  <li class="list-group-item">빛나는 백청 행운부적</li>
				  <li class="list-group-item">빛나는 수월 행운부적</li>
				  <li class="list-group-item">포화란의 향수</li>
				  
	  		  </ul>
	        </div>
	    </div>
	  </div>
	</div>
   </div><!-- /input-group -->
<h4>서버</h4>
	<div class="input-group">
      <input type="text" class="form-control" name="servername" placeholder="서버명을 입력해라묘">
  </div><!-- /input-group -->

<h4>거래구분</h4>
  <div class="btn-group" data-toggle="buttons">
  <label class="btn btn-primary active">
    <input type="radio" name="div" id="option1" autocomplete="off" value="1" checked> 판매
  </label>
  <label class="btn btn-primary">
    <input type="radio" name="div" id="option2" autocomplete="off" value="2"> 구매
  </label>
</div>
<button type="button" id="search-btn" style="margin-top: 10px; width:100%"  
data-loading-text="시장가 검색 중..." class="btn btn-primary">
  검색
</button>
<hr>

<div class="contents"></div>
</div>
</body>
<script>
	$(document).ready(function() 
	{
		document.getElementById("auction").className = "active";
	    $('ul.list-group li').click(function(e) 
	    { 
	    	$('input[name=itemname]').val($(this).text());
	    	$('#modal').modal('hide');
	    });
	});
	$('#search-btn').click(function ()
	{
		var btn = $(this);
		var xmlhttp;
	
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4) {
				btn.button('reset');
				if (xmlhttp.status == 200) {
					//document.querySelector('.contents').innerHTML = xmlhttp.responseText;
					var result = JSON.parse(xmlhttp.responseText);
					var strbuff = '<table class="table" style="max-width:500px"><colgroup><col width="20%"><col width="20%"><col width="15%"><col width="15%"><col width="10%"></colgroup><col width="20%"></colgroup><thead><tr><th style="text-align: center;">등록일시</th><th  style="text-align: center;">아이템명</th><th style="text-align: center;">가격</th><th style="text-align: center;">시세차</th><th style="text-align: center;">개수</th><th style="text-align: center;">캐릭명</th></tr></thead><tbody>';
					for(var i=0; i<result.length; i++){
						strbuff += '<tr>'
					+ '<td style="text-align: right;">'
					+ result[i].date
					+ '</td><td style="text-align: right;">'
					+ result[i].itemname
					+ '</td><td style="text-align: right;">'
					+ result[i].price
					+ '</td><td style="text-align: right;">'
					+ result[i].marketprice
					+ '</td><td style="text-align: right;">'
					+ result[i].num
					+ '</td><td style="text-align: right;">'
					+ result[i].username
					+ '</td></tr>';
					}
					document.querySelector('.contents').innerHTML = strbuff;
					
				}
			}
	
		}
		var itemName = $('input[name=itemname]').val();
		var serverName = $('input[name=servername]').val();
		var div = $('input[name=div]:checked').val();

		var params = "ITEMNAME=" + encodeURIComponent(itemName) + "&SERVERNAME=" + encodeURIComponent(serverName) + "&DIV=" + div;

		btn.button('loading');
		xmlhttp.open("POST", "Market", true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send(params);
	});
</script>
</html>