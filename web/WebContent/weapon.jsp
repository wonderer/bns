<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>배온-장비정보</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<!-- Custom CSS -->
<link href="css/simple-sidebar.css" rel="stylesheet">


<style>
.input-group { padding-bottom:5px; width:100%}
.plusitem { text-align:center;}
.minusitem { width:40px; text-align:center;}
</style>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53425022-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<div id="wrapper">
	<div id="sidebar-wrapper">
		<%@ include file="sidemenu.jsp" %> 
	</div>
	<!-- /#sidebar-wrapper -->
<div id="page-content-wrapper">

	<%@ include file="menu.jsp" %> 

	<div class="container" style="width:100%">
		<h4>무기 종류</h4>
			<select name="ITEMKIND" id = "kind" class="form-control" style="width:100%;">
			<option value="legend">전설무기</option>
			<option value="weapon_muchon">무천트리</option>
			<option value="weapon_gaksung">각성극마트리</option>
			</select>
		<button type="button" id="search-btn" style="margin-top: 10px; width:100%"  
			data-loading-text="시장가 검색 중이다묘..." class="btn btn-primary">
	  	검색
		</button>
		<hr>
		<div class="contents"></div>
	</div>
</div>
</div>
</body>
<script>
$(document).ready(function() 
{
	$('#title').text('무기정보');
});
$('#search-btn').click(function ()
{
	var btn = $(this);
	btn.button('loading');
	var xmlhttp;
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	{

		if (xmlhttp.readyState==4)
		{
		 	if( xmlhttp.status==200 )
		 	{
		 		btn.button('reset');
		 		document.querySelector('.contents').innerHTML = xmlhttp.responseText;
		 		$('[name=detail]').click(function ()
				{
		 			var id = '[id=' + $(this).val() + ']';
		 			$(id).toggle();
				});
		 	}
		}
  
	}
	xmlhttp.open("GET","Equip?ITEMKIND="+document.getElementById("kind").value,true);
	xmlhttp.send();
});

</script>
</html>