<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>배온-블소장터</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="./js/custom_modal.js"></script>
<!-- Custom CSS -->
<link href="css/simple-sidebar.css" rel="stylesheet">
<link href="css/toastr.min.css" rel="stylesheet">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53425022-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<div id="wrapper">
	<div id="sidebar-wrapper">
		<%@ include file="sidemenu.jsp" %> 
	</div>
	<!-- /#sidebar-wrapper -->
<div id="page-content-wrapper">
<%@ include file="menu.jsp" %>
<div class="container" style="width:100%">

	<h4>제작단</h4>
	<select name="ITEMKIND" id = "kind" class="form-control" style="width:100%;">
	  <option value="mangum">만금당</option>
	  <option value="taesang">태상문</option>
	  <option value="cholmu">철무방</option>
	  <option value="sunggun">성군당</option>
	  <option value="dogi">도기방</option>
	  <option value="yakwang">약왕원</option>
	  <option value="ilmi">일미문</option>
	  
	<!-- 
	  
	 -->  
	</select>
	<h4>검색옵션</h4>
	<div class="btn-group" data-toggle="buttons">
		<label class="btn btn-primary active">
			<input type="checkbox" autocomplete="off" id="check_smalltrade" checked> 적은 거래량 표시
		</label>
		<label class="btn btn-primary">
    		<input type="checkbox" autocomplete="off" id="check_minusitem" > 마이너스 물품 표시
  		</label>
	</div>
	<!--  <button type="submit" class="btn btn-default" style="
	    margin-top: 10px;
	">Submit</button> -->
	<button type="button" id="search-btn" style="margin-top: 10px; width:100%"  
	data-loading-text="시장가 검색 중이다묘..." class="btn btn-primary">
	  검색
	</button>
	<hr>
	<div class="contents">
		<div style="text-align:center;margin:20px"><img id="loading_img" src="./img/m_main1.png" style="
    			width: 200px;
  	"></div>
	
	</div>
</div>
</div>
</div>
<!-- popup notice -->
<div id="modal-id" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">공지사항</h4>
                </div>
                <div class="modal-body">
                	<p>제작정보에서 제작하기 버튼을 누르면 당시의 재료비를 기억해주는 기능을 추가하였습니다.</p>
			    	<p>제작이력에 들어가면 당시에 재료비와 현재 판매했을 때의 차액을 계산해줍니다.</p>
			    	<p>가격변동폭이 심한 경우에 실제 만들 때 알아본 차익과 팔 때의 차익이 많이 달라지는 경우가 생겨서 추가해보았어요.</p>
			    	<hr>
			    	<p>2015.2.27</p>
			    	<p>* 제작 이력 보존 기능을 추가하였습니다.</p>
			    	<hr>
			    	<p>2015.2.25</p>
			    	<p>* 무극 목걸이 트리를 추가하였습니다.</p>
			    	<hr>
			    	<p>2015.2.15</p>
			    	<p>* 제작 화면에서 즐겨찾기 등록한 물품을 최상단에 표시되게 하였습니다.</p>
			    	<p>* 무극 귀걸이 트리를 추가하였습니다.</p>
			    	<hr>
			    	<p>2015.2.13</p>
			    	<p>* 각성 악녀 트리를 추가하였습니다.</p>
			    	<hr>
			    	<p>2015.2.12</p>
			    	<p>* 각성 극마무기 트리를 추가하였습니다.</p>
			    	<hr>
			    	<p>2015.2.9</p>
			    	<p>* 무천무기, 전설무기 트리를 추가하였습니다.</p>
			    	<p>* 불멸반지 트리를 추가하였습니다.</p>
			    	<p>* 장비정보표시에 제물과 비용은 기본표시되도록 변경하였습니다.</p>
			    	<hr>
			    	<p>2015.2.8</p>
			    	<p>* 전설보패를 추가하였습니다.
			    	<hr>
			    	<p>2015.2.6</p>
			    	<p>* 제작 즐겨찾기 기능을 추가하였습니다.
			    	<p>* 카툰 4화가  추가하였습니다.
			    	<p>* 일부 단말에서 스크롤이 끝까지 안되는 문제를 수정하였습니다.
			    	<hr>
			    	<p>2015.2.4</p>
			    	<p>* 명인, 거장 합성패를 추가하였습니다.
			    	<hr>
			    	<p>2015.2.1</p>
			    	<p>* 시장거래수수료를 수정하였습니다.
			    	<hr>
			    	<p>2015.1.31</p>
			    	<p>* 메뉴가 사이드메뉴로 개편되었습니다.
			    	<br>* 간이경매에서 아이템 목록에 얼음창고 구슬 아이템이 추가되었습니다.</p>
			    	<hr>
			    	<p>2015.1.27</p>
			    	<p>* 악세정보(귀걸이,목걸이,반지)가 추가되었습니다.(도움주신분:<a href="http://sandbox.plaync.com/FD3D6CAF-674F-E211-958F-E61F135E992F" target="_blank">릴리s</a>)
			    	<br>* 악세정보에 누적재료비 항목을 추가하였습니다.</p>
			    	<hr>
			    	<p>2015.1.26</p>
			    	<p>* 악세정보(팔찌,허리띠)가 추가되었습니다.</p>
			    	<hr>
			    	<p>2015.1.25</p>
			    	<p>* 카툰 3화가 추가되었습니다.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" style="width:100px" data-dismiss="modal">닫기</button>
                </div>
            </div>
        </div>
    </div>
</body>
<script>
var favoriteList = {};
var itemList = new Array();

var makeList = {};
var makeItemList = new Array();

var searchType;
$(document).ready(function() 
{
	
	$('#title').text('제작정보');
	var version = getCookie("VERSION");
	if( version!=16 )
	{
		Show("modal-id");	
		setCookie("VERSION", 16, 365);
	}
	
});
//For todays date;
Date.prototype.today = function () { 
    return this.getFullYear() +"/"+(((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"/"+ ((this.getDate() < 10)?"0":"") + this.getDate();
}

// For the time now
Date.prototype.timeNow = function () {
     return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
}


$('#search-btn').click(function ()
{
	var btn = $(this);
	btn.button('loading');
	var xmlhttp;
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	{

		if (xmlhttp.readyState==4)
		{
			btn.button('reset');
		 	if( xmlhttp.status==200 )
		 	{
		 		toastr.options = {
		 				  "closeButton": false,
		 				  "debug": false,
		 				  "newestOnTop": false,
		 				  "progressBar": false,
		 				  "positionClass":  "toast-bottom-full-width",
		 				  "preventDuplicates": false,
		 				  "onclick": null,
		 				  "showDuration": "300",
		 				  "hideDuration": "1000",
		 				  "timeOut": "2500",
		 				  "extendedTimeOut": "1000",
		 				  "showEasing": "swing",
		 				  "hideEasing": "linear",
		 				  "showMethod": "fadeIn",
		 				  "hideMethod": "fadeOut"
		 				};
		 		document.querySelector('.contents').innerHTML = xmlhttp.responseText;
		 		$('[name=detail]').click(function ()
				{
		 			var id = '[id=' + $(this).attr('value') + ']';
		 			$(id).toggle();
		 			$(this).toggleClass("toggled");
		 			if( $(this).hasClass("toggled" ) )
		 			{
		 				$(this).attr('src', "./img/btn_more_close.png");	
		 			}
		 			else
		 			{
		 				$(this).attr('src', "./img/btn_more.png");
		 			}

				});

		 		$('[name=savemake]').click(function ()
				{
		 			var itemInfo = {};
	 				itemInfo['name'] = $(this).attr('value');
	 				itemInfo['cost'] = $(this).attr('value2');
	 				var newDate = new Date();
	 				itemInfo['time'] = newDate.today() + "  " + newDate.timeNow();
	 				makeItemList.push(itemInfo);
	 				makeList[searchType] = makeItemList;
		 			var object = JSON.stringify(makeList);
		 			setCookie("MAKEINFO", encodeURIComponent(object), 365);
		 			toastr["success"]("제작이력에 등록하였다묘!");
		 			console.log(object);
				});
		 		$('[name=favorite]').click(function ()
				{
		 			
		 			
		 			$(this).toggleClass("toggled");
		 			if( $(this).hasClass("toggled" ) )
		 			{
		 				$(this).attr('src', "./img/icon_favor_on.png");	
		 				//즐겨찾기 추가
		 				var itemInfo = {};
		 				itemInfo['name'] = $(this).attr('value');
		 				itemList.push(itemInfo);
		 				toastr["success"]("즐겨찾기에 등록하였다묘!");
		 			}
		 			else
		 			{
		 				$(this).attr('src', "./img/icon_favor_off.png");
		 				//즐겨찾기 제거
		 				
		 				for( var i in itemList )
		 				{
		 					if( itemList[i].name ==  $(this).attr('value') )
		 					{
		 						itemList.splice(i,1);
		 						toastr["success"]("즐겨찾기에서 제외하였다묘!");
		 						break;
		 					}
		 				}
		 				
		 			}
		 			//쿠키 저장
		 			favoriteList[searchType] = itemList;
		 			var object = JSON.stringify(favoriteList);
		 			setCookie("FAVORITE", encodeURIComponent(object), 365);
				});
		 	}
		}
  
	}
	
	searchType = document.getElementById("kind").value;

	
	var param = "ITEMKIND="+searchType;
	var param1 = "&SMALLTRADE=";
	var param2 = "&MINUSITEM=";
	var param3 = "&FAVLIST=";
	
	if( $('input[id=check_smalltrade]:checked').val() != null )
	{
		param += param1 + "1";
	}
	if(  $('input[id=check_minusitem]:checked').val() != null )
	{
		param += param2+ "1";
	}
	//쿠키에서 즐겨찾기 읽어오기
	var favor = getCookie("FAVORITE");
	if( favor != "" ) 
	{
		favoriteList = JSON.parse(decodeURIComponent(favor));
		var list = favoriteList[searchType];
		if( list != null )
		{
			itemList = list;
			param += param3 + encodeURIComponent(JSON.stringify(itemList));
		}
	}

	var makeInfo = getCookie("MAKEINFO");
	if( makeInfo != "" ) 
	{
		makeList = JSON.parse(decodeURIComponent(makeInfo));
		var list = makeList[searchType];
		if( list != null )
		{
			makeItemList = list;
			param += param3 + encodeURIComponent(JSON.stringify(makeItemList));
		}
	}
	console.log(JSON.stringify(makeItemList));
	document.querySelector('.contents').innerHTML = '<div style="text-align:center;margin:20px"><img id="loading_img" src="./img/m_loading.png" style="width: 200px;"></div>';
	xmlhttp.open("POST", "Make", true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send(param);

	
});
</script>
<script src="./js/cookie.js"></script>
<script src="./js/toastr.min.js"></script>
</html>