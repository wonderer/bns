<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>배온-간이경매</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Custom CSS -->
<link href="css/simple-sidebar.css" rel="stylesheet">

<style>
.btn-group { width:100%; }
.btn-primary { width:20%; }
</style>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53425022-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<div id="wrapper">
	<div id="sidebar-wrapper">
		<%@ include file="sidemenu.jsp" %> 
	</div>
	<!-- /#sidebar-wrapper -->
<div id="page-content-wrapper">
<%@ include file="menu.jsp" %> 
<div class="container" style="width:100%">
<h4>인원</h4>
<div class="btn-group" id="people" data-toggle="buttons">
  <label class="btn btn-primary">
    <input type="radio" name="partynum" id="option1" value="2"> 2
  </label>
  <label class="btn btn-primary">
    <input type="radio" name="partynum" id="option2" value="3"> 3
  </label>
  <label class="btn btn-primary active">
    <input type="radio" name="partynum" id="option3"  value="4" checked> 4
  </label>
  <label class="btn btn-primary">
    <input type="radio" name="partynum" id="option4" value="5"> 5
  </label>
  <label class="btn btn-primary">
    <input type="radio" name="partynum" id="option5" value="6"> 6
  </label>
</div>
<h4>아이템</h4>
   <div class="input-group">
     <input type="text" class="form-control" name="itemname" placeholder="아이템명을 입력해라묘">
     <span class="input-group-btn">
       <button class="btn btn-default" type="button" data-toggle="modal" data-target=".bs-example-modal-sm">목록에서 검색</button>
     </span>
   </div><!-- /input-group -->
<h4>개수</h4>
	<div class="input-group">
      <input type="number" class="form-control" name="itemnum" min="0" value="0">
      <span class="input-group-btn">
        <button class="btn btn-default" type="button" name="plus" id="plus1" value="1">+1</button>
        <button class="btn btn-default" type="button" name="plus" id="plus5" value="5">+5</button>
        <button class="btn btn-default" type="button" name="plus" id="plus10" value="10">+10</button>
        <button class="btn btn-default" type="button" name="plus" id="reset" value="1">reset</button>
      </span>
    </div><!-- /input-group -->
    
<button type="button" id="search-btn" style="margin-top: 10px; width:100%"  
data-loading-text="시장가 검색 중..." class="btn btn-primary">
  검색
</button>
<hr>

<div class="alert alert-danger fade in" role="alert" id="name_alert" style="display:none">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      <strong>아이템명</strong> 을 입력해라묘.
</div>
<div class="alert alert-danger fade in" role="alert" id="num_alert" style="display:none">
      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
      <strong>아이템 개수</strong> 를 입력해라묘.
</div>

<div class="contents"></div>
</div>
</div>
</div>
     <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="modal">
  	 <div class="modal-dialog modal-sm">
	    <div class="modal-content">
		    <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
	          <h4 class="modal-title" id="mySmallModalLabel">아이템을 선택해라묘</h4>
	        </div>
		    <div class="modal-body">
	          <ul class="list-group">
	          	  <li class="list-group-item">녹색 구슬</li>
	          	  <li class="list-group-item">청색 구슬</li>
	          	  <li class="list-group-item">적색 구슬</li>
				  <li class="list-group-item">얼어붙은 독침</li>
				  <li class="list-group-item">해무진의 혼</li>
				  <li class="list-group-item">봉인된 나류석판</li>
				  <li class="list-group-item">빛나는 백청 행운부적</li>
				  <li class="list-group-item">빛나는 수월 행운부적</li>
	  		  </ul>
	        </div>
	    </div>
	  </div>
	</div>
</body>
<script>
	$(document).ready(function() 
	{
		$('#title').text('간이경매');
	    $('ul.list-group li').click(function(e) 
	    { 
	    	$('input[name=itemname]').val($(this).text());
	    	$('#modal').modal('hide');
	    });
	    
	});
	function plusClick(clickButton)
	{
		var numInputbox = $('input[name=itemnum]');
		var curr = Number(numInputbox.val());
		var plus = Number(clickButton.val());
		curr += plus;
		numInputbox.val(curr);
	}
	
	$('[name=plus]').click(function ()
	{
		plusClick($(this));	
	});
	
	$('#reset').click(function ()
	{
		var numInputbox = $('input[name=itemnum]');
		numInputbox.val(0);
	});
	$('#search-btn').click(function ()
	{
		var btn = $(this);
		var xmlhttp;
	
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4) {
				btn.button('reset');
				if (xmlhttp.status == 200) {
					document.querySelector('.contents').innerHTML = xmlhttp.responseText;
				}
			}
	
		}
		var partyNum = $('input[name=partynum]:checked').val();
		var itemName = $('input[name=itemname]').val();
		var itemNum = $('input[name=itemnum]').val();
		if( !itemName )
		{
			$("#name_alert").show();
		}
		else
		{
			$("#name_alert").hide();
		}
		if( itemNum == 0 )
		{
			$("#num_alert").show();
		}
		else
		{
			$("#num_alert").hide();
		}
		
		if( itemName && itemNum > 0)
		{
			btn.button('loading');
			$(".alert").alert('close');
			
			var params = "PARTYNUM="+ partyNum + "&ITEMNAME="+ encodeURIComponent(itemName) + "&ITEMNUM=" + itemNum;
			xmlhttp.open("POST", "Auction", true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send(params);
		}
	});
</script>
</html>