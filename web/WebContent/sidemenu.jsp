<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8"%>
<header>
 <ul class="sidebar-nav">
	        <li class="sidebar-brand">
	            <img src="./img/logo.png" style="
    			width: 120px;
				">
	        </li>
	        <li>
	            <a href="make.jsp"><img src="./img/icon_01.png" style="
    			width: 20px;
    			margin-right: 10px;
				">제작정보</a>
	        </li>
	        <li>
	            <a href="favorite.jsp"><img src="./img/icon_01.png" style="
    			width: 20px;
    			margin-right: 10px;
				">즐겨찾는 제작</a>
	        </li>
	        <li>
	            <a href="makehistory.jsp"><img src="./img/icon_01.png" style="
    			width: 20px;
    			margin-right: 10px;
				">제작이력  <img src="./img/icon_new.png" style="
    			width: 15px;
				"></a>
	        </li>
	        <li>
	            <a href="auction.jsp"><img src="./img/icon_02.png" style="
    			width: 20px;
    			margin-right: 10px;
				">간이경매</a>
	        </li>
	        <li>
	            <a href="bope.jsp"><img src="./img/icon_03.png" style="
    			width: 20px;
    			margin-right: 10px;
				">보패정보</a>
	        </li>
	        <li>
	            <a href="weapon.jsp"><img src="./img/icon_04.png" style="
    			width: 20px;
    			margin-right: 10px;
				">무기정보</a>
	        </li>
	        <li>
	            <a href="accessory.jsp"><img src="./img/icon_04.png" style="
    			width: 20px;
    			margin-right: 10px;
				">악세정보</a>
	        </li>
	        <li>
	            <a href="calc.jsp"><img src="./img/icon_05.png" style="
    			width: 20px;
    			margin-right: 10px;
				">재료 계산기</a>
	        </li>
	        <li>
	            <a href="cartoon.jsp"><img src="./img/icon_06.png" style="
    			width: 20px;
    			margin-right: 10px;
				">카툰</a>
	            
	        </li>
	        <li>
	            <a href="http://sandbox.plaync.com/5B3C93F8-D15F-E011-9A06-E61F135E992F" target="_blank"><img src="./img/icon_07.png" style="
    			width: 20px;
    			margin-right: 10px;
				">개발자 샌드박스</a>
	        </li>
	    </ul>
</header>
