<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>배온-블소장터</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="./js/custom_modal.js"></script>
<!-- Custom CSS -->
<link href="css/simple-sidebar.css" rel="stylesheet">
<link href="css/toastr.min.css" rel="stylesheet">

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53425022-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<div id="wrapper">
	<div id="sidebar-wrapper">
		<%@ include file="sidemenu.jsp" %> 
	</div>
	<!-- /#sidebar-wrapper -->
<div id="page-content-wrapper">
<%@ include file="menu.jsp" %>
<div class="container" style="width:100%">

	<!--  <button type="submit" class="btn btn-default" style="
	    margin-top: 10px;
	">Submit</button> -->
	<div class="contents"></div>
</div>
</div>
</div>
</body>
<script>
var makeList = {};


$(document).ready(function() 
{
	$('#title').text('제작이력');
	var xmlhttp;
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4)
		{
			
		 	if( xmlhttp.status==200 )
		 	{
		 		toastr.options = {
		 				  "closeButton": false,
		 				  "debug": false,
		 				  "newestOnTop": false,
		 				  "progressBar": false,
		 				  "positionClass":  "toast-bottom-full-width",
		 				  "preventDuplicates": false,
		 				  "onclick": null,
		 				  "showDuration": "300",
		 				  "hideDuration": "1000",
		 				  "timeOut": "2500",
		 				  "extendedTimeOut": "1000",
		 				  "showEasing": "swing",
		 				  "hideEasing": "linear",
		 				  "showMethod": "fadeIn",
		 				  "hideMethod": "fadeOut"
		 				};
		 		if( xmlhttp.responseText == "" )
		 		{
		 			var top = $(document).height()/3 +"px";
		 			document.querySelector('.contents').innerHTML = '<h4 style="text-align: center;margin-top:' + top + '">등록된 물품이 없다묘ㅠ.ㅠ</h4>';	
		 		}
		 		else
		 		{
		 			document.querySelector('.contents').innerHTML = xmlhttp.responseText;
		 		}
		 		$('[name=detail]').click(function ()
				{
		 			var id = '[id=' + $(this).attr('value') + ']';
		 			$(id).toggle();
		 			$(this).toggleClass("toggled");
		 			if( $(this).hasClass("toggled" ) )
		 			{
		 				$(this).attr('src', "./img/btn_more_close.png");	
		 			}
		 			else
		 			{
		 				$(this).attr('src', "./img/btn_more.png");
		 			}

				});
		 		$('[name=deletemake]').click(function ()
				{
		 			var type = $(this).attr('value2');
		 			var itemList = makeList[type];
	 				for( var i in itemList )
	 				{
	 					if( itemList[i].name ==  $(this).attr('value') && itemList[i].time ==  $(this).attr('value1') )
	 					{
	 						itemList.splice(i,1);
	 						//쿠키 저장
	 			 			makeList[type] = itemList;
	 			 			var object = JSON.stringify(makeList);
	 			 			setCookie("MAKEINFO", encodeURIComponent(object), 365);
	 			 			toastr["success"]("제작이력에서 제외하였다묘!");
	 			 			requestData(xmlhttp);
	 						break;
	 					}
	 				}
	 				
				});
		 	}
		}
	}
	
	requestData(xmlhttp);
	
	
	
});
function requestData(xmlhttp)
{
	var makeInfo = getCookie("MAKEINFO");
	if( makeInfo != "" ) 
	{
		makeList = JSON.parse(decodeURIComponent(makeInfo));
		var object = JSON.stringify(makeList);
		
		var params = "MAKEITEMLIST=" + encodeURIComponent(object);

		xmlhttp.open("POST", "MakeHistory", true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send(params);

	}
	else
	{
		var top = $(document).height()/3 +"px";
		
		document.querySelector('.contents').innerHTML = '<h4 style="text-align: center;margin-top:' + top + '">등록된 물품이 없다묘ㅠ.ㅠ</h4>';	
	}
}
</script>
<script src="./js/cookie.js"></script>
<script src="./js/toastr.min.js"></script>
</html>