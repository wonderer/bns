<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>배온-재료계산기</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Custom CSS -->
<link href="css/simple-sidebar.css" rel="stylesheet">

<style>
.input-group { padding-bottom:5px; width:100%}
.plusitem { text-align:center;}
.minusitem { width:40px; text-align:center;}
</style>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53425022-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<div id="wrapper">
	<div id="sidebar-wrapper">
		<%@ include file="sidemenu.jsp" %> 
	</div>
	<!-- /#sidebar-wrapper -->
<div id="page-content-wrapper">

<%@ include file="menu.jsp" %> 
<div class="container" style="width:100%">
<br>
<div class="itemlist panel panel-default">
  	<div class="panel-heading">
  		<div class="input-group">
		     <input type="text" class="form-control" id="groupname-text-input" name="groupname" value="Group1">
		     <span class="input-group-btn">
		       <button class="btn btn-default" id="group-save-button" type="button">저장</button>
		       <button class="btn btn-default" id="group-load-button" type="button">읽기</button>
		     </span>
		</div>
	</div>
	<div class="panel-body">
    </div>
    <button type="button" id="itemadd-btn" style="margin-top: 10px; width:100%" class="plusitem btn btn-default">
  		아이템 추가
	</button>
</div>
<button type="button" id="search-btn" style="margin-top: 10px; width:100%"  
data-loading-text="시장가 검색 중..." class="btn btn-primary">
  검색
</button>
<hr>
<div class="contents"></div>
</div>
</div>
</div>
    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="modal">
  	 <div class="modal-dialog modal-sm">
	    <div class="modal-content">
		    <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
	          <h4 class="modal-title" id="mySmallModalLabel">아이템을 선택해라묘</h4>
	        </div>
		    <div class="modal-body">
	          <ul class="list-group">
				  <li class="list-group-item">얼어붙은 독침</li>
				  <li class="list-group-item">해무진의 혼</li>
				  <li class="list-group-item">봉인된 나류석판</li>
				  <li class="list-group-item">빛나는 백청 행운부적</li>
				  <li class="list-group-item">빛나는 수월 행운부적</li>
				  <li class="list-group-item">포화란의 향수</li>
	  		  </ul>
	        </div>
	    </div>
	  </div>
	</div>

</body>
<script>
	var buttonindex;
	$(document).ready(function() 
	{
		$('#title').text('재료 계산기');
		addItem();

		$('[name=plus1]').click(function ()
		{
			var name = 'itemnum' + $(this).parent().attr('value');
			plusClick($(this), name);	
		});
		
		$('#reset1').click(function ()
		{
			var name = 'itemnum' + $(this).parent().attr('value');
			var numInputbox = $('input[name=' + name + ']');
			numInputbox.val(0);
		});
		$('#group-save-button').click(function(e) 
   	    { 

   	    	var totalList = {};
			var itemList = new Array();
			
			for( var i=1; i< $('input[type=text]').length; i++ )
			{
				var itemInfo = {};
				 
				itemInfo["name"] =  $('input[type=text]').get(i).value;
				itemInfo["num"] = $('input[type=number]').get(i-1).value;
				itemList.push(itemInfo);
			}
			var groupName = $('input[type=text]').get(0).value; 

			totalList['groupname'] = groupName;
			totalList['itemlist'] = itemList;

			var object = JSON.stringify(totalList);
			var encoding = encodeURIComponent(object);

			saveItems(encoding);
			
   	    });
   	    $('#group-load-button').click(function(e) 
   	    { 
   	    	var saveditem = loadItems();
   	    	if( saveditem != '')
   	    	{
   	    		reloadItems(saveditem);
   	    	}
   	    		
   	    });
	    $('ul.list-group li').click(function(e) 
   	    { 
   	    	$('input[name=itemname' + buttonindex + ']').val($(this).text());
   	    	$('#modal').modal('hide');
   	    });
   		$('button[name=listbutton1]').click(function(e) 
   	    { 
   	    	buttonindex = $(this).val();
   	    });
		$(".plusitem").click(function () {
			addItem();
		});
		
		$('#search-btn').click(function ()
	    { 
	    	var btn = $(this);
			var xmlhttp;
		
			if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			} else {// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4) {
					btn.button('reset');
					if (xmlhttp.status == 200) {
						document.querySelector('.contents').innerHTML = xmlhttp.responseText;
						window.scrollTo(0,document.body.scrollHeight);
					}
				}
		
			}
			var totalList = {};
			var itemList = new Array();
			
			for( var i=1; i< $('input[type=text]').length; i++ )
			{
				var itemInfo = {};
				if( $('input[type=text]').get(i).value != "" && $('input[type=number]').get(i-1).value != "")
				{
					itemInfo["name"] =  $('input[type=text]').get(i).value;
					itemInfo["num"] = $('input[type=number]').get(i-1).value;
					itemList.push(itemInfo);
				}
			}
			var groupName = $('input[type=text]').get(0).value; 

			totalList['groupname'] = groupName;
			totalList['itemlist'] = itemList;

			var object = JSON.stringify(totalList);
			var params = "ITEMLIST=" + encodeURIComponent(object);

			btn.button('loading');
			xmlhttp.open("POST", "Calc", true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send(params);
			
	    });
	});
	function plusClick(clickButton, name)
	{
		var numInputbox = $('input[name='+name+']');
		var curr = Number(numInputbox.val());
		var plus = Number(clickButton.val());
		curr += plus;
		numInputbox.val(curr);
	}
	function reloadItems( saveditem ) {
		var itemlist = JSON.parse(decodeURIComponent(saveditem));

	    $('#groupname-text-input').val(itemlist['groupname']);

		$('.item').remove();
    	for( var i=0; i< itemlist['itemlist'].length; i++ )
    	{
    		addItem();
    		
    		$('[name=itemname' + i + ']').val(itemlist['itemlist'][i]['name']);
			$('[name=itemnum' + i + ']').val(itemlist['itemlist'][i]['num']);
    	}
	}
	function saveItems( data ) {
		if( navigator.userAgent.match(/Android/i) ) {
			//alert('android');
			window.Native.saveItems( data );
		}
		else if( navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) )
		{
			//alert('ios');
			var arg = "bnshelper://calc/saveitems=" + data;
			alert(window.location);
			window.location = arg;
		}
		else
		{
			//alert('cookie');
			setCookie("ITEMLIST", data, 365);
		}

	}


	function loadItems() {
		var items = '';
		if( navigator.userAgent.match(/Android/i) )
		{
			items = window.Native.loadItems( "" );
		}
		else if( navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) )
		{
			window.location = "bnshelper://calc/loaditems=";
		} 
		else
		{
			items = getCookie("ITEMLIST");
		}
		return items;
	}
	function addItem() {
			var size = $(".item").size();
			var innerHTML = '<div class="item" id="input-group' + size + '">' + 
								'<h5>Item' + (size+1) + '</h5>' +
								'<div class="input-group">' +
								     '<input type="text" class="form-control"  placeholder="아이템명을 입력해라묘" name="itemname' + size + '">' +
								     '<span class="input-group-btn">' +
								       '<button value="' + size + '" name="listbutton' + size + '" class="btn btn-default" type="button" data-toggle="modal" data-target=".bs-example-modal-sm">목록에서 검색</button>' +
								     '</span>' +
								'</div>' +
								'<div class="input-group">' +
							      '<input type="number" class="form-control" name="itemnum' + size + '" min="0" value="0">' +
							      '<span class="input-group-btn" value=' + size + '>' +
							        '<button class="btn btn-default" type="button" name="plus' + size + '" id="plus1" value="1">+1</button>' +
							        '<button class="btn btn-default" type="button" name="plus' + size + '" id="plus5" value="5">+5</button>' +
							        '<button class="btn btn-default" type="button" name="plus' + size + '" id="plus10" value="10">+10</button>' +
							        '<button class="btn btn-default" type="button" id="reset' + size + '" >reset</button>' +
							      '</span>' +
							    '</div>' +
							'</div>';
			$(".panel-body").append(innerHTML);
			$("#minusbutton" + size).click(function () {
				var index = $(this).val();
				$("#input-group" + index).remove();
			});
			$('button[name=listbutton' + size + ']').click(function(e) 
	   	    { 
	   	    	buttonindex = $(this).val();
	   	    });
	   	    $('[name=plus' + size + ']').click(function ()
			{
				var name = 'itemnum' + $(this).parent().attr('value');
				plusClick($(this), name);	
			});
			
			$('#reset' + size + '').click(function ()
			{
				var name = 'itemnum' + $(this).parent().attr('value');
				var numInputbox = $('input[name=' + name + ']');
				numInputbox.val(0);
			});
	}
</script>
<script src="./js/cookie.js"></script>
</html>