<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>배온-블소장터</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="./js/custom_modal.js"></script>
<!-- Custom CSS -->
<link href="css/simple-sidebar.css" rel="stylesheet">
<link href="css/toastr.min.css" rel="stylesheet">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53425022-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<div id="wrapper">
	<div id="sidebar-wrapper">
		<%@ include file="sidemenu.jsp" %> 
	</div>
	<!-- /#sidebar-wrapper -->
<div id="page-content-wrapper">
<%@ include file="menu.jsp" %>
<div class="container" style="width:100%">

	<!--  <button type="submit" class="btn btn-default" style="
	    margin-top: 10px;
	">Submit</button> -->
	<div class="contents"></div>
</div>
</div>
</div>
</body>
<script>
var favoriteList = {};
var makeList = {};
var makeItemList = new Array();
//For todays date;
Date.prototype.today = function () { 
    return this.getFullYear() +"/"+(((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"/"+ ((this.getDate() < 10)?"0":"") + this.getDate();
}

// For the time now
Date.prototype.timeNow = function () {
     return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
}


$(document).ready(function() 
{
	$('#title').text('즐겨찾는 제작');
	var xmlhttp;
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4)
		{
			
		 	if( xmlhttp.status==200 )
		 	{
		 		if( xmlhttp.responseText == "" )
		 		{
		 			var top = $(document).height()/3 +"px";
		 			document.querySelector('.contents').innerHTML = '<h4 style="text-align: center;margin-top:' + top + '">등록된 물품이 없다묘ㅠ.ㅠ</h4>';	
		 		}
		 		else
		 		{
		 			toastr.options = {
			 				  "closeButton": false,
			 				  "debug": false,
			 				  "newestOnTop": false,
			 				  "progressBar": false,
			 				  "positionClass":  "toast-bottom-full-width",
			 				  "preventDuplicates": false,
			 				  "onclick": null,
			 				  "showDuration": "300",
			 				  "hideDuration": "1000",
			 				  "timeOut": "2500",
			 				  "extendedTimeOut": "1000",
			 				  "showEasing": "swing",
			 				  "hideEasing": "linear",
			 				  "showMethod": "fadeIn",
			 				  "hideMethod": "fadeOut"
			 				};
		 			
		 			document.querySelector('.contents').innerHTML = xmlhttp.responseText;
			 		$('[name=detail]').click(function ()
					{
			 			var id = '[id=' + $(this).attr('value') + ']';
			 			$(id).toggle();
			 			$(this).toggleClass("toggled");
			 			if( $(this).hasClass("toggled" ) )
			 			{
			 				$(this).attr('src', "./img/btn_more_close.png");	
			 			}
			 			else
			 			{
			 				$(this).attr('src', "./img/btn_more.png");
			 			}

					});
			 		$('[name=savemake]').click(function ()
					{
			 			var type = $(this).attr('value1');
			 			var itemInfo = {};
		 				itemInfo['name'] = $(this).attr('value');
		 				itemInfo['cost'] = $(this).attr('value2');
		 				var newDate = new Date();
		 				itemInfo['time'] = newDate.today() + "  " + newDate.timeNow();
		 				makeItemList.push(itemInfo);
		 				makeList[type] = makeItemList;
			 			var object = JSON.stringify(makeList);
			 			setCookie("MAKEINFO", encodeURIComponent(object), 365);
			 			toastr["success"]("제작이력에 등록하였다묘!");
			 			console.log(object);
					});
			 		$('[name=favorite]').click(function ()
						{
			 				var type = $(this).attr('value1');
					 			$(this).toggleClass("toggled");
					 			if( $(this).hasClass("toggled" ) )
					 			{
					 				$(this).attr('src', "./img/icon_favor_on.png");	
					 				//즐겨찾기 추가
					 				var itemInfo = {};
					 				itemInfo['name'] = $(this).attr('value');
					 				var itemList = favoriteList[type];
					 				itemList.push(itemInfo);
					 			}
					 			else
					 			{
					 				$(this).attr('src', "./img/icon_favor_off.png");
					 				//즐겨찾기 제거
					 				
					 				var itemList = favoriteList[type];
					 				for( var i in itemList )
					 				{
					 					if( itemList[i].name ==  $(this).attr('value') )
					 					{
					 						itemList.splice(i,1);
					 						break;
					 					}
					 				}
					 				
					 			}
					 			//쿠키 저장
					 			favoriteList[type] = itemList;
					 			var object = JSON.stringify(favoriteList);
					 			setCookie("FAVORITE", encodeURIComponent(object), 365);
							});
		 		}
		 	
		 	}
		}
	}
	
	var favor = getCookie("FAVORITE");
	if( favor != "" ) 
	{
		favoriteList = JSON.parse(decodeURIComponent(favor));
		var object = JSON.stringify(favoriteList);
		
		var params = "ITEMLIST=" + encodeURIComponent(object);

		xmlhttp.open("POST", "Favorite", true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send(params);

	}
	else
	{
		var top = $(document).height()/3 +"px";
		
		document.querySelector('.contents').innerHTML = '<h4 style="text-align: center;margin-top:' + top + '">등록된 물품이 없다묘ㅠ.ㅠ</h4>';	
	}
	
	
	
});
</script>
<script src="./js/cookie.js"></script>
<script src="./js/toastr.min.js"></script>
</html>