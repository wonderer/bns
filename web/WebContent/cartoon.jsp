<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>배온-카툰</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- Custom CSS -->
<link href="css/simple-sidebar.css" rel="stylesheet">

<style>
.btn-group { width:100%; }
.btn-primary { width:20%; }
img {width:100%;}
</style>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53425022-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<div id="wrapper">
	<div id="sidebar-wrapper">
		<%@ include file="sidemenu.jsp" %> 
	</div>
	<!-- /#sidebar-wrapper -->
<div id="page-content-wrapper">

<%@ include file="menu.jsp" %> 
<div class="container" style="width:100%">
<br>
<div style="text-align:center;margin:20px"><img src="./img/toon_top.png" style="
    			width: 200px;
  "></div>
    
  
  <div class="panel" style="color: #747474;">
    <span style="color:#52caca">배부르면 온순해요</span> 문파는 2012년 공명정대 '은하전장' 문파로 시작되어 현재 경국지색 '배부르면 온순해요' 문파로 이어지는
끈끈한 인연을 맺고 있는 라이트 유저 문파다묘. <br><br>20대 초반의 풋풋한 아가씨부터 40대 잔소리 할아버지까지 다양한 분이 게임을
즐기고 있다묘. 2년이상 게임을 했지만 늘지 않는 실력, 헤딩으로 단련된 강인한 멘탈이 문파의 특징이다묘?
  <br><br>
  <p><span style="
    background: #52caca;
    color: white;
    padding: 5px;
    margin-top: 20px;">illust by 꿀물</span></p>
  </div>
<hr>
<h4>문파툰</h4>
<div class="btn-group" id="number" data-toggle="buttons" style="width:100%">
  <label class="btn btn-primary" id="v1"  style="width:25%">
    <input type="radio" name="partynum" id="option1" value="1"> 1화
  </label>
  <label class="btn btn-primary" id="v2" style="width:25%">
    <input type="radio" name="partynum" id="option2" value="2"> 2화
  </label>
  <label class="btn btn-primary" id="v3" style="width:25%">
    <input type="radio" name="partynum" id="option3" value="3"> 3화
  </label>
  <label class="btn btn-primary" id="v4" style="width:25%">
    <input type="radio" name="partynum" id="option4" value="4"> 4화
  </label>
  
</div>
<hr>
<div class="contents">
<div class="viewArticle" id="article_contents">
	<div class="vol1" style="display:none">
		<img class="editorAttachedImage" src="http://imgfiles.plaync.co.kr/file/BladeNSoul/download/20130917025805878-14123cb3182--73c00-v4" style="cursor:hand;cursor:pointer;" alt="">
		<img class="editorAttachedImage" src="http://imgfiles.plaync.co.kr/file/BladeNSoul/download/20130917025813be-14123ae2c3e--73700-v4" style="cursor:hand;cursor:pointer;" alt="">
		<img class="editorAttachedImage" src="http://imgfiles.plaync.co.kr/file/BladeNSoul/download/20130917025822878-14123cb3182--73bf0-v4" style="cursor:hand;cursor:pointer;" alt="">
	</div>
	<div class="vol2" style="display:none">	
		<img class="editorAttachedImage" src="http://imgfiles.plaync.co.kr/file/BladeNSoul/download/20131022115024d8-141dd140d15--7ebc0-v4" style="cursor:hand;cursor:pointer;" alt="">
		<img class="editorAttachedImage" src="http://imgfiles.plaync.co.kr/file/BladeNSoul/download/20131022115035636-141dd0b6396--7ede0-v4" style="cursor:hand;cursor:pointer;" alt="">
		<img class="editorAttachedImage" src="http://imgfiles.plaync.co.kr/file/BladeNSoul/download/20131022115039d0-141dd1c17dc--7ec70-v4" style="cursor:hand;cursor:pointer;" alt="">
		<img class="editorAttachedImage" src="http://imgfiles.plaync.co.kr/file/BladeNSoul/download/20131022120348d0-141dd1c17dc--7eaf0-v4" style="cursor:hand;cursor:pointer;" alt="">
	</div>
	<div class="vol3" style="display:none">	
		<img class="editorAttachedImage" src="http://imgfiles.plaync.com/file/BladeNSoul/download/20150119141041-14aff1a9cd4--7df40-v4" style="cursor:hand;cursor:pointer;" alt="">
		<img class="editorAttachedImage" src="http://imgfiles.plaync.com/file/BladeNSoul/download/2015011914105068-14aff35a9aa--7de90-v4" style="cursor:hand;cursor:pointer;" alt="">
	</div>
	<div class="vol4" style="display:none">	
		<img class="editorAttachedImage" src="http://imgfiles.plaync.com/file/BladeNSoul/download_mobile/2015020618053153-14b5bcd8b41--7c790-v4" style="cursor:hand;cursor:pointer;" alt="">
		<img class="editorAttachedImage" src="http://imgfiles.plaync.com/file/BladeNSoul/download_mobile/2015020618053160-14b5be0a9bb--7caf0-v4" style="cursor:hand;cursor:pointer;" alt="">
		<img class="editorAttachedImage" src="http://imgfiles.plaync.com/file/BladeNSoul/download_mobile/201502061806061-14b5bc653ec--7c970-v4" style="cursor:hand;cursor:pointer;" alt="">
		
	</div>
</div>
</div>
</div>
</div>
</div>
</body>
<script>
$(document).ready(function() 
{
	$('#title').text('카툰');
});

$('label').click(function(e) {
    e.preventDefault();
});
$('#v1').click(function ()
{
	$(".vol1").show();
	$(".vol2").hide();
	$(".vol3").hide();
	$(".vol4").hide();
});
$('#v2').click(function ()
{
	$(".vol1").hide();
	$(".vol2").show();
	$(".vol3").hide();
	$(".vol4").hide();
});
$('#v3').click(function ()
{
	$(".vol1").hide();
	$(".vol2").hide();
	$(".vol3").show();
	$(".vol4").hide();
});
$('#v4').click(function ()
{
	$(".vol1").hide();
	$(".vol2").hide();
	$(".vol3").hide();
	$(".vol4").show();
});
	
</script>
</html>