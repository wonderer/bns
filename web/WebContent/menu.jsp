<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8"%>
<link href="css/custom.css" rel="stylesheet">
<header>
 <nav class="titlebar" style="
    background-color: #333;
    height: 45px;
    line-height: 45px;
    text-align: center; 
    position: fixed;
    min-width: 100%;
    z-index: 20;">
	<img src="./img/icon_side.png" style="
    width: 20px;
    height: 20px;
    float: left;
    margin-top: 12.5px;
    margin-left: 12.5px;" 
    id="menuicon" >
	<span id="title" style="
    color: #fff;
    font-size: large;
    margin-left: -32.5px;"
">제작정보</span>
</nav>
</header>
<div class="mask" style="display: none; width: 100%; height: 970px;background-color: rgba(0, 0, 0, 0.7);position: fixed;padding-top: 45px;z-index: 100;">
</div>
<script>
$(document).ready(function() 
{
	var body = document.body,
    html = document.documentElement;

	var height = Math.max( body.scrollHeight, body.offsetHeight, 
                       html.clientHeight, html.scrollHeight, html.offsetHeight );
	$('.container').css("height", height);

});


$('#menuicon').click(function ()
{
	$("#wrapper").toggleClass("toggled");
	$(".mask").toggle();
});
$('.mask').click(function ()
{
	$("#wrapper").toggleClass("toggled");
	$(".mask").toggle();

});
</script>

