package kr.playparty.bns.equip;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import kr.playparty.bns.item.MaterialItemInfo;

public class EquipInfo {

	private static HashMap<String, ArrayList<EquipInfo>> hash= new HashMap<String, ArrayList<EquipInfo>>();
	private String itemName;
	private int makeCost;
	private int materialPrice;			//재료비
	
	
	private ArrayList<MaterialItemInfo> materialItemList = new ArrayList<MaterialItemInfo>();
	public static void loadData(String path, String fileName) {
		if( hash.get(fileName) == null )
		{
			File f = new File(path+"/data/" +fileName +".csv");
			try {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(new FileInputStream(f), "UTF-8"));
				
				ArrayList<EquipInfo> list = parseInputData(reader);
				reader.close();
				hash.put(fileName, list);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	private static ArrayList<EquipInfo> parseInputData(BufferedReader reader) throws IOException {
		ArrayList<EquipInfo> list = new ArrayList<EquipInfo>();

		EquipInfo itemInfo = null;
		String line;
		while(true)
		{
			line = reader.readLine();
			if( line == null ) {
				list.add(itemInfo);
				break;
			}
			
			
			String[] splitLines = line.split(",");
			if( splitLines[0].equals("완성품") )
			{
				if( itemInfo != null )
				{
					list.add(itemInfo);
				}
				itemInfo = new EquipInfo();
				itemInfo.setItemName(splitLines[1]);
			}
			else
			{
				MaterialItemInfo materialItemInfo = new MaterialItemInfo();
				if( splitLines[1].equals("비용"))
				{
					itemInfo.setMakeCost(Integer.parseInt(splitLines[2]));
				}
				else
				{
					if( splitLines[0].equals("일반제물"))
					{
						materialItemInfo.setExactSearch(false);
					}
					materialItemInfo.setItemName(splitLines[1]);
					materialItemInfo.setItemNum(Integer.parseInt(splitLines[2]));
					itemInfo.materialItemList.add(materialItemInfo);
				}
			}
		}
		return list;
	}
	public static ArrayList<EquipInfo> getDataList(String fileName) {
		return hash.get(fileName);
	}
	public String getItemName() {
		return itemName;
	}
	private void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public ArrayList<MaterialItemInfo> getMaterialItemList() {
		return materialItemList;
	}
	public int getMaterialPrice() {
		return materialPrice;
	}
	public void setMaterialPrice(int materialPrice) {
		this.materialPrice = materialPrice;
	}
	public int getMakeCost() {
		return makeCost;
	}
	private void setMakeCost(int makeCost) {
		this.makeCost = makeCost;
	}
}
