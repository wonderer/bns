package kr.playparty.bns;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.playparty.bns.item.ItemManager;
import kr.playparty.bns.item.MaterialItemInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Servlet implementation class Market
 */
public class Market extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Market() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();

		String itemName = request.getParameter("ITEMNAME");
		String serverName = request.getParameter("SERVERNAME");
		String div = request.getParameter("DIV");
		
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName(Const.JDBC_DRIVER);
			conn = (Connection) DriverManager.getConnection(Const.getDBURL(), Const.DB_ACCOUNT, Const.DB_PASSWORD);
			stmt = (Statement) conn.createStatement();
			ResultSet rs = stmt
					.executeQuery("SELECT * FROM market where div1='" + div
							+ "' and itemname='" + itemName
							+ "' and servername='" + serverName + "' order by updated_at desc; ");
			
			String num = null;
			String price = null;
			String userName = null;
			String date = null;
			MaterialItemInfo itemInfo = ItemManager.searchMarketPrice(itemName, true, true);
			int marketPrice = itemInfo.getPrice();
			
			JSONArray jsonArray = new JSONArray();
			
			while(rs.next()) {
    			num = rs.getString("num");
    			price = rs.getString("price");
    			userName = rs.getString("username");
    			date = rs.getString("updated_at");
    			int deltaPrice = Integer.parseInt(price) - marketPrice;
    			
    			
    			JSONObject retData = new JSONObject();
    			try {
    				retData.put("div", div);
    				retData.put("itemname", itemName);
    				retData.put("num", num);
    				retData.put("price", ItemManager.getPriceString(Integer.parseInt(price)));
    				retData.put("marketprice", ItemManager.getPriceString(deltaPrice));
    				retData.put("username", userName);
    				retData.put("date", date);
    				
    			} catch (JSONException e) {
    				e.printStackTrace();
    			}
    			jsonArray.put(retData);
		    }
		
			
			out.print(jsonArray.toString());
		} catch (Exception e) {
			e.printStackTrace();
		} finally
		{
			try {
				stmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
