package kr.playparty.bns;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.playparty.bns.item.ItemManager;
import kr.playparty.bns.item.MadeItemInfo;
import kr.playparty.bns.item.MaterialItemInfo;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Servlet implementation class MakeHistory
 */
public class MakeHistory extends HttpServlet {
	private static final String ILMI = "ilmi";
	private static final String YAKWANG = "yakwang";
	private static final String DOGI = "dogi";
	private static final String SUNGGUN = "sunggun";
	private static final String CHOLMU = "cholmu";
	private static final String MANGUM = "mangum";
	private static final String TAESANG = "taesang";
	private static final long serialVersionUID = 1L;
	private static final int WARNING_TRADE_NUM = 50;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MakeHistory() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();

		String items = request.getParameter("MAKEITEMLIST");
		JSONObject itemGroup = new JSONObject(items);
		int startItemIndex = 0;
		String[] typeList = {TAESANG, MANGUM, CHOLMU, SUNGGUN, DOGI, YAKWANG, ILMI};
		for( String type: typeList )
		{
			if( itemGroup.has(type) )
			{
				ArrayList<MadeItemInfo> madeItemList = getMadeItemInfo(itemGroup, type);
				if( madeItemList.size() > 0 )
				{
					displayMadeItemInfo(out, startItemIndex, madeItemList, type);
					startItemIndex += madeItemList.size();
				}
			}
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	private ArrayList<MadeItemInfo> getMadeItemInfo(JSONObject itemGroup, String type) {
		
		JSONArray favList = itemGroup.getJSONArray(type);
		MadeItemInfo.loadData(getServletContext().getRealPath(""), type);
		ArrayList<MadeItemInfo> dataList = MadeItemInfo.getDataList(type);
		ArrayList<MadeItemInfo> madeItemList = new ArrayList<MadeItemInfo>();
		
		for( int i=0; i < favList.length(); i++ )
		{
			JSONObject object  = favList.getJSONObject(i);
			String name = object.getString("name");
			for (MadeItemInfo madeItemInfo : dataList) 
			{
				if( madeItemInfo.getItemName().equals(name) )
				{
					madeItemInfo.setMaterialPrice(object.getInt("cost"));
					madeItemInfo.setMakeTime(object.getString("time"));
					madeItemList.add(madeItemInfo);
					break;
				}
			}
		}
		return madeItemList;
	}

	private void displayMadeItemInfo(PrintWriter out, int itemIndex,
			ArrayList<MadeItemInfo> madeItemList, String type) throws IOException {
		out.println("<h5 style=\"text-align: center;background: darkblue;font-size: 20px;color: white;padding: 5px;\">" + getTypeName(type) + "</h5>");
		
		for (MadeItemInfo madeItemInfo : madeItemList) {

			MaterialItemInfo item = ItemManager.getItemInfo(madeItemInfo.getItemName(), true, false);
			madeItemInfo.setMadeItemPrice(item.getPrice());
			madeItemInfo.setTradeNum(item.getTradeNum());
			
			itemIndex++;
			out.println("<h4 style=\"float:left\">" + madeItemInfo.getItemName() + "("	+ madeItemInfo.getItemNum() + "개)</h4>");
			out.println("<img src=\"./img/btn_more.png\" style=\"width: 30px;margin-top: 10px;float: right;\" class=\"\" name=\"detail\" value=\"item"+itemIndex+"\">");
			out.println("<button type=\"button\" style=\"width: 100%\" class=\"btn btn-default btn-sm\" name=\"deletemake\" value=\""+madeItemInfo.getItemName()+"\" value1=\""+madeItemInfo.getMakeTime()+"\" value2=\"" + type + "\" >판매완료</button>");
			out.println("<table class=\"table\" style=\"max-width:500px\"><colgroup><col width=\"50%\"><col width=\"15	%\"><col width=\"35%\"></colgroup><thead><tr><th style=\"text-align: center;\">항목</th><th  style=\"text-align: center;\">개수</th><th style=\"text-align: center;\">금액(개당)</th></tr></thead><tbody>");

			out.println("<tr><td>재료비(제작당시)</td><td colspan=2 style=\"text-align: right;\">"
					+ ItemManager.getPriceString(madeItemInfo.getMaterialPrice())
					+ "</td></tr>");
			out.println("<tr  style=\"display: none\"  id=\"item" + itemIndex +"\"><td>제작비</td><td colspan=2 style=\"text-align: right;\">"
					+ ItemManager.getPriceString(madeItemInfo.getMakeCost())
					+ "</td></tr>");

			String style2 = null;
			String madeItemPrice = ItemManager.getPriceString(madeItemInfo.getTotalMadeItemPrice());
			if( madeItemInfo.getTotalMadeItemPrice() == 0 )
			{
				style2 = "class=\"danger\"";
				madeItemPrice = "검색불가";
			}
			

			out.println("<tr  style=\"display: none\"  id=\"item" + itemIndex +"\"><td>판매수수료("
					+ (madeItemInfo.getSaleTaxRatio() * 100)
					+ "%)</td><td colspan=2 style=\"text-align: right;\">"
					+ ItemManager.getPriceString(madeItemInfo.getSaleTaxPrice()) + "</td></tr>");

			out.println("<tr  style=\"display: none\"  id=\"item" + itemIndex +"\"><td>일거래수수료("
					+ (madeItemInfo.getDailyTaxRatio() * 100)
					+ "%)</td><td colspan=2 style=\"text-align: right;\">"
					+ ItemManager.getPriceString(madeItemInfo.getDailyTaxPrice()) + "</td></tr>");
			out.println("<tr " + style2 + "><td>판매가(최저가)</td><td colspan=2 style=\"text-align: right;\">"
					+ madeItemPrice
					+ "</td></tr>");

			String gainClass = "success";
			String gainTitle = "현재차익";
			
			if (madeItemInfo.getGain() < 0) {
				gainClass = "danger";
			} 
			out.println("<tr class=\""
					+ gainClass
					+ "\"><td colspan=2>" + gainTitle +"</td><td style=\"text-align: right;\">"
					+ ItemManager.getPriceString(madeItemInfo.getGain()) + "</td></tr>");
			out.println("</tbody></table>");
			out.println("<h5 style=\"text-align:right\">제작일시 : " + madeItemInfo.getMakeTime() + "</h5>");

			out.println("<hr>");
		}
	}

	private String getTypeName(String type) {
		String ret = "";
		
		if( type.equals(ILMI) )
		{
			ret = "일미문";
		}
		else if( type.equals(YAKWANG) )
		{
			ret = "약왕원";
		}
		else if( type.equals(DOGI) )
		{
			ret = "도기방";
		}
		else if( type.equals(SUNGGUN) )
		{
			ret = "성군당";
		}
		else if( type.equals(CHOLMU) )
		{
			ret = "철무방";
		}
		else if( type.equals(MANGUM) )
		{
			ret = "만금당";
		}
		else if( type.equals(TAESANG) )
		{
			ret = "태상문";
		}
		return ret;
	}
}
