package kr.playparty.bns.bope;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;


public class BopeInfo {
	
	private static HashMap<String,ArrayList<BopeInfo>> hashMap = new HashMap<String,ArrayList<BopeInfo>>();
	
	private String name;
	private int number;
	private String mainOption;
	private String subOption;

	private int maxComposeNum;
	
	public static void loadData(String path, String fileName)
	{
		if( hashMap.size() == 0 )
		{
			parseInputData(path, fileName);
		}
		
	}

	private static void parseInputData(String path, String fileName) {
		File f = new File(path+"/data/" +fileName +".csv");
		try {
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(new FileInputStream(f), "UTF-8"));
			ArrayList<BopeInfo> list = new ArrayList<BopeInfo>();
			String bopeName = null;
			BopeInfo bopeInfo = null;
			String line;
			while(true)
			{
				line = reader.readLine();
				if( line == null ) {
					hashMap.put(bopeInfo.name, list);
					break;
				}
				
				
				String[] splitLines = line.split(",");
				if( splitLines[0].equals("이름") )
				{
					if( bopeInfo != null )
					{
						hashMap.put(bopeInfo.name, list);
						list = new ArrayList<BopeInfo>();
					}
					bopeName = splitLines[1];
				}
				else
				{
					bopeInfo = new BopeInfo();
					bopeInfo.setName(bopeName);
					int number = Integer.parseInt(splitLines[0]);
					bopeInfo.setNumber(number);
					bopeInfo.setMainOption(splitLines[1]);
					bopeInfo.setSubOption(splitLines[2]);
					bopeInfo.setMaxComposeNum(Integer.parseInt(splitLines[3]));
					list.add(bopeInfo);

				}
			}
			reader.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMainOption() {
		return mainOption;
	}

	public void setMainOption(String mainOption) {
		this.mainOption = mainOption;
	}

	public String getSubOption() {
		return subOption;
	}

	public void setSubOption(String subOption) {
		this.subOption = subOption;
	}

	public static ArrayList<BopeInfo> getBopeInfo(String bopeName) {
		return hashMap.get(bopeName);
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
	public void setMaxComposeNum(int maxComposeNum) {
		this.maxComposeNum = maxComposeNum;
	}
	public int getMaxComposeNum() {
		return maxComposeNum;
	}
}
