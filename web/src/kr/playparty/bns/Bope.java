package kr.playparty.bns;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.playparty.bns.bope.BopeInfo;

/**
 * Servlet implementation class Bope
 */
public class Bope extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Bope() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		
		String bopeName = request.getParameter("NAME");

		BopeInfo.loadData(getServletContext().getRealPath(""), "bope");
		ArrayList<BopeInfo> info = BopeInfo.getBopeInfo(bopeName);
		out.println("<h4>"+info.get(0).getName()+"</h4>");
		
		out.println("<table class=\"table\" style=\"max-width:500px\"><colgroup><col width=\"5%\"><col width=\"35%\"><col width=\"60%\"></colgroup><thead><tr><th style=\"text-align: center;\">No</th><th  style=\"text-align: center;\">주옵</th><th style=\"text-align: center;\">부옵</th></tr></thead><tbody>");
		
		for (BopeInfo bopeInfo : info) {
			out.println("<tr><td style=\"text-align: center;\">" + bopeInfo.getNumber()
					+ "</td><td style=\"text-align: right;\">"
					+ bopeInfo.getMainOption()
					+ "</td><td style=\"text-align: right;\">"
					+ bopeInfo.getSubOption()
					+ "</td></tr>");
//			out.println(bopeInfo.getNumber()+":"+bopeInfo.getMainOption()+ " " + bopeInfo.getSubOption()+"<br>");
		}
		
		out.println("<table class=\"table table-bordered\" style=\"max-width:500px\"><colgroup><col width=\"5%\"><col width=\"20%\"><col width=\"5%\"><col width=\"20%\"><col width=\"5%\"><col width=\"20%\"><col width=\"5%\"><col width=\"20%\"></colgroup><tbody>");
		out.println("<h4>최대 합성치</h4>");
		
		out.println("<tr>"
				+ "<td style=\"text-align: center;\" class=\"active\">" 
				+ "1"
				+ "</td>"
				+ "<td style=\"text-align: center;\">"
				+ info.get(0).getMaxComposeNum()
				+ "</td>"
				+ "<td style=\"text-align: center;\" class=\"active\">" 
				+ "2"
				+ "</td>"
				+ "<td style=\"text-align: center;\">"
				+ info.get(1).getMaxComposeNum()
				+ "</td>"+ "<td style=\"text-align: center;\" class=\"active\">" 
				+ "3"
				+ "</td>"
				+ "<td style=\"text-align: center;\">"
				+ info.get(2).getMaxComposeNum()
				+ "</td>"+ "<td style=\"text-align: center;\" class=\"active\">" 
				+ "4"
				+ "</td>"
				+ "<td style=\"text-align: center;\">"
				+ info.get(3).getMaxComposeNum()
				+ "</td>"
				+ "</tr>");
	
		out.println("<tr>"
				+ "<td style=\"text-align: center;\" class=\"active\">" 
				+ "5"
				+ "</td>"
				+ "<td style=\"text-align: center;\">"
				+ info.get(4).getMaxComposeNum()
				+ "</td>"
				+ "<td style=\"text-align: center;\" class=\"active\">" 
				+ "6"
				+ "</td>"
				+ "<td style=\"text-align: center;\">"
				+ info.get(5).getMaxComposeNum()
				+ "</td>"+ "<td style=\"text-align: center;\" class=\"active\">" 
				+ "7"
				+ "</td>"
				+ "<td style=\"text-align: center;\">"
				+ info.get(6).getMaxComposeNum()
				+ "</td>"+ "<td style=\"text-align: center;\" class=\"active\">" 
				+ "8"
				+ "</td>"
				+ "<td style=\"text-align: center;\">"
				+ info.get(7).getMaxComposeNum()
				+ "</td>"
				+ "</tr>");
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
