package kr.playparty.bns;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.playparty.bns.equip.EquipInfo;
import kr.playparty.bns.item.ItemManager;
import kr.playparty.bns.item.MadeItemInfo;
import kr.playparty.bns.item.MaterialItemInfo;

/**
 * Servlet implementation class equip
 */
public class Equip extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Equip() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		
		String fileName = request.getParameter("ITEMKIND");
		
		EquipInfo.loadData(getServletContext().getRealPath(""), fileName);
		ArrayList<EquipInfo> dataList = EquipInfo.getDataList(fileName);
		int itemIndex = 0;
		int totalCost = 0;
		for (EquipInfo equipInfo : dataList) {
			itemIndex++;
			out.println("<div style=\"text-align:center\"><h3 style=\"background-color: #18709c; color: #FFF6ee;\">" + equipInfo.getItemName() + "</h3>");
			//out.println("<p>|</p>");
			if( equipInfo.getMaterialItemList().size() > 0 )
			{
				int index2 = 0;
				String hidden = "style=\"display: none\"  id=\"item" + itemIndex +"\"";

				
				equipInfo.setMaterialPrice(getTotalMaterialPrice(equipInfo));
				
				//out.println("<p>돌파비용 보기</p>"); style=\"display: none\"  id=\"item" + itemIndex +"\
				out.println("<button type=\"button\" style=\"width: 100%\" class=\"btn btn-default btn-sm\" name=\"detail\" value=\"item"+itemIndex+"\">자세히 보기</button>");
				out.println("<p></p>");
				out.println("<table class=\"table table-striped\" style=\"max-width:500px;\"><colgroup><col width=\"45%\"><col width=\"15%\"><col width=\"40%\"></colgroup><thead><tr><th style=\"text-align: center;\">재료</th><th  style=\"text-align: center;\">개수</th><th style=\"text-align: center;\">금액(개당)</th></tr></thead><tbody>");
				for (MaterialItemInfo materialItemInfo : equipInfo.getMaterialItemList()) {
					index2++;
					String style = "";
					String materialPrice = ItemManager.getPriceString(materialItemInfo.getPrice());
					if( materialItemInfo.getPrice() == 0 )
					{
						style = "class=\"danger\"";
						materialPrice = "검색불가";
					}
					if( index2 > 1 )
					{
						style = hidden+style;
					}
					out.println("<tr " + style + "><td>" + materialItemInfo.getItemName()
							+ "</td><td style=\"text-align: center;\">"
							+ materialItemInfo.getItemNum()
							+ "</td><td style=\"text-align: right;\">"
							+ materialPrice
							+ "</td></tr>");
					
				}
				out.println("<tr " + hidden + "><td>돌파비</td><td colspan=2 style=\"text-align: right;\">"
						+ ItemManager.getPriceString(equipInfo.getMakeCost())
						+ "</td></tr>");

				out.println("<tr class=\"success\"><td>재료비(총합)</td><td colspan=2 style=\"text-align: right;\">"
						+ ItemManager.getPriceString(equipInfo.getMaterialPrice()+equipInfo.getMakeCost())
						+ "</td></tr>");
				totalCost += equipInfo.getMaterialPrice()+equipInfo.getMakeCost();
				out.println("<tr " + hidden + "class=\"success\"><td>누적재료비(총합)</td><td colspan=2 style=\"text-align: right;\">"
						+ ItemManager.getPriceString(totalCost)
						+ "</td></tr>");

				out.println("</tbody></table>");
				if( itemIndex < dataList.size() )
				{
					out.println("<span class=\"glyphicon glyphicon-arrow-down\"></span>");
				}
			}
			out.println("</div>");
		}
		
		
		
		
		
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	private int getTotalMaterialPrice(EquipInfo itemInfo) {
		int totalMaterialPrice = 0;
		for (MaterialItemInfo materialItemInfo : itemInfo
				.getMaterialItemList()) {
			int avg1 = 0;
			try {
				avg1 = ItemManager.getItemInfo(materialItemInfo.getItemName(), materialItemInfo.isExactSearch(), true).getPrice();
				materialItemInfo.setPrice(avg1);
			} catch (Exception e) {
				e.printStackTrace();
			}
			totalMaterialPrice += avg1 * materialItemInfo.getItemNum();
		}
		return totalMaterialPrice;
	}

}
