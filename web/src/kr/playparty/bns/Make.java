package kr.playparty.bns;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import kr.playparty.bns.item.ItemManager;
import kr.playparty.bns.item.MadeItemInfo;
import kr.playparty.bns.item.MaterialItemInfo;


/**
 * Servlet implementation class make
 */
public class Make extends HttpServlet {
	public static final int WARNING_TRADE_NUM = 50;
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Make() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();

		long startTime = System.currentTimeMillis();

		String fileName = request.getParameter("ITEMKIND");
		boolean isSmallTrade = request.getParameter("SMALLTRADE") == null ? false : true;
		boolean isMinusItem = request.getParameter("MINUSITEM") == null ? false : true;;
		String favList = request.getParameter("FAVLIST");
		JSONArray favorite = null;
		if( favList != null )
		{
			favorite = new JSONArray(favList);
		}
		
		
		
		MadeItemInfo.loadData(getServletContext().getRealPath(""), fileName);
		ArrayList<MadeItemInfo> dataList = MadeItemInfo.getDataList(fileName);
		ArrayList<MadeItemInfo> favoriteList = MadeItemInfo.getDataList(favorite, fileName);

		for (MadeItemInfo madeItemInfo : dataList) {

			madeItemInfo.setMaterialPrice(getTotalMaterialPrice(madeItemInfo));
			MaterialItemInfo item = ItemManager.getItemInfo(madeItemInfo.getItemName(), true, false);
			madeItemInfo.setMadeItemPrice(item.getPrice());
			madeItemInfo.setTradeNum(item.getTradeNum());
		}

		Collections.sort(dataList, new MadeItemInfo.GainCompare());
		Collections.sort(favoriteList, new MadeItemInfo.GainCompare());
		long searchtime = System.currentTimeMillis() - startTime;
		out.println("<div><small style=\"float:right;\">검색시간 : " + searchtime / 1000 + " sec</small><br></div>");
		if( favoriteList.size() > 0 )
		{
			out.println("<div style=\"text-align:center\"><h4 style=\"background-color: #18709c; color: #FFF6ee; padding:5px;\">즐겨찾는 물품</h4>");
			displayList(out, true, true, favorite, favoriteList, 0 );
		}
		
		out.println("<div style=\"text-align:center\"><h4 style=\"background-color: #18709c; color: #FFF6ee; padding:5px;\">모든 물품</h4>");
		displayList(out, isSmallTrade, isMinusItem, favorite, dataList, favoriteList.size());
		out.close();

	}

	private void displayList(PrintWriter out,boolean isSmallTrade,
			boolean isMinusItem, JSONArray favorite,
			ArrayList<MadeItemInfo> dataList, int startIndex) {
		int itemIndex = startIndex;
		for (MadeItemInfo madeItemInfo : dataList) {
			if( isSmallTrade == false && madeItemInfo.getTradeNum() < WARNING_TRADE_NUM)
			{
				continue;
			}
			if( isMinusItem == false && madeItemInfo.getGain() < 0)
			{
				continue;
			}
			itemIndex++;
			String favoriteHTML = "<div><img src=\"./img/icon_favor_off.png\" style=\"width: 20px;margin-top: 10px;margin-right: 10px;float: left;\" class=\"\" name=\"favorite\" value=\""+madeItemInfo.getItemName()+"\"><h4 style=\"float:left\">" + madeItemInfo.getItemName() + "("	+ madeItemInfo.getItemNum() + "개)</h4>";
			for( int i = 0; favorite != null && i < favorite.length(); i++ )
			{
				JSONObject object = favorite.getJSONObject(i);
				if( object.get("name").equals(madeItemInfo.getItemName()) )
				{
					favoriteHTML = "<div><img src=\"./img/icon_favor_on.png\" style=\"width: 20px;margin-top: 10px;margin-right: 10px;float: left;\" class=\"toggled\" name=\"favorite\" value=\""+madeItemInfo.getItemName()+"\"><h4 style=\"float:left\">" + madeItemInfo.getItemName() + "("	+ madeItemInfo.getItemNum() + "개)</h4>";
					break;
				}
			}
			out.println(favoriteHTML);
//			out.println("<button  style=\"float:right; background: url(./img/btn_more.png) no-repeat;background-size: contain;width: 59;width: 36px;height: 23px;border: none;margin-top: 9px;\" type=\"button\" name=\"detail\" value=\"item"+itemIndex+"\"></button>");
			out.println("<img src=\"./img/btn_more.png\" style=\"width: 30px;margin-top: 10px;float: right;\" class=\"\" name=\"detail\" value=\"item"+itemIndex+"\">");
			out.println("<button type=\"button\" style=\"width: 100%\" class=\"btn btn-default btn-sm\" name=\"savemake\" value=\""+madeItemInfo.getItemName()+"\" value2=\""+madeItemInfo.getMaterialPrice()+"\">제작하기</button>");
			out.println("<table class=\"table\" style=\"max-width:500px\"><colgroup><col width=\"50%\"><col width=\"15	%\"><col width=\"35%\"></colgroup><thead><tr><th style=\"text-align: center;\">항목</th><th  style=\"text-align: center;\">개수</th><th style=\"text-align: center;\">금액(개당)</th></tr></thead><tbody>");

			boolean isAllSearchSuccess = true;
			for (MaterialItemInfo materialItemInfo : madeItemInfo
					.getMaterialItemList()) {
				String style = "";
				String materialPrice = ItemManager.getPriceString(materialItemInfo.getPrice());
				if( materialItemInfo.getPrice() == 0 )
				{
					style = "class=\"danger\"";
					materialPrice = "검색불가";
					isAllSearchSuccess = false;
				}
				style += " style=\"display: none\" id=\"item" + itemIndex + "\"";
				out.println("<tr " + style + "><td>" + materialItemInfo.getItemName()
						+ "</td><td style=\"text-align: center;\">"
						+ materialItemInfo.getItemNum()
						+ "</td><td style=\"text-align: right;\">"
						+ materialPrice
						+ "</td></tr>");
			}

			out.println("<tr><td>재료비(총합)</td><td colspan=2 style=\"text-align: right;\">"
					+ ItemManager.getPriceString(madeItemInfo.getMaterialPrice())
					+ "</td></tr>");

			out.println("<tr style=\"display: none\"  id=\"item" + itemIndex +"\"><td>제작비</td><td colspan=2 style=\"text-align: right;\">"
					+ ItemManager.getPriceString(madeItemInfo.getMakeCost())
					+ "</td></tr>");

			String style2 = null;
			String madeItemPrice = ItemManager.getPriceString(madeItemInfo.getTotalMadeItemPrice());
			if( madeItemInfo.getTotalMadeItemPrice() == 0 )
			{
				style2 = "class=\"danger\"";
				madeItemPrice = "검색불가";
			}
			

			out.println("<tr style=\"display: none\" id=\"item" + itemIndex +"\"><td>판매수수료("
					+ (madeItemInfo.getSaleTaxRatio() * 100)
					+ "%)</td><td colspan=2 style=\"text-align: right;\">"
					+ ItemManager.getPriceString(madeItemInfo.getSaleTaxPrice()) + "</td></tr>");

			out.println("<tr style=\"display: none\" id=\"item" + itemIndex +"\"><td>일거래수수료("
					+ (madeItemInfo.getDailyTaxRatio() * 100)
					+ "%)</td><td colspan=2 style=\"text-align: right;\">"
					+ ItemManager.getPriceString(madeItemInfo.getDailyTaxPrice()) + "</td></tr>");
			out.println("<tr " + style2 + "><td>판매가(최저가)</td><td colspan=2 style=\"text-align: right;\">"
					+ madeItemPrice
					+ "</td></tr>");

			String gainClass = "success";
			String gainTitle = "차익";
			
			if (madeItemInfo.getGain() < 0) {
				gainClass = "danger";
			} else if (madeItemInfo.getTradeNum() < WARNING_TRADE_NUM) {
				gainClass = "warning";
				gainTitle += " <span class=\"label label-warning\">거래량 주의</span>";
				
			}
			if( isAllSearchSuccess == false )
			{
				gainClass = "warning";
				gainTitle += " <span class=\"label label-danger\">재료비 주의</span>";
			}
			else if (madeItemInfo.getGain() > 1000000 )
			{
				gainClass = "warning";
				gainTitle += " <span class=\"label label-warning\">지나친 차익</span>";
				
			}

			out.println("<tr class=\""
					+ gainClass
					+ "\" style=\"display: none\" id=\"item" + itemIndex + "\"><td>거래량</td><td colspan=2 style=\"text-align: right;\">"
					+ madeItemInfo.getTradeNum()
					+ "건</td></tr>");

			out.println("<tr class=\""
					+ gainClass
					+ "\"><td  colspan=2 >" + gainTitle +"</td><td style=\"text-align: right;\">"
					+ ItemManager.getPriceString(madeItemInfo.getGain()) + "</td></tr>");
			out.println("</tbody></table>");
		}
	}

	private int getTotalMaterialPrice(MadeItemInfo madeItemInfo) {
		int totalMaterialPrice = 0;
		for (MaterialItemInfo materialItemInfo : madeItemInfo
				.getMaterialItemList()) {
			int avg1 = 0;
			try {
				avg1 = ItemManager.getItemInfo(materialItemInfo.getItemName(), materialItemInfo.isExactSearch(),true).getPrice();
				materialItemInfo.setPrice(avg1);
			} catch (Exception e) {
				e.printStackTrace();
			}
			totalMaterialPrice += avg1 * materialItemInfo.getItemNum();
		}
		return totalMaterialPrice;
	}



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
