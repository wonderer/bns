package kr.playparty.bns;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.playparty.bns.item.ItemManager;
import kr.playparty.bns.item.MaterialItemInfo;

/**
 * Servlet implementation class Auction
 */
public class Auction extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Auction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();

		int partyNum = Integer.parseInt(request.getParameter("PARTYNUM"));
		String itemName = request.getParameter("ITEMNAME");
		int itemNum = Integer.parseInt(request.getParameter("ITEMNUM"));
		
		processAuctionMinPrice(out, partyNum, itemName, itemNum);
	}

	private void processAuctionMinPrice(PrintWriter out, int partyNum,
			String itemName, int itemNum) throws IOException {
		MaterialItemInfo itemInfo = ItemManager.searchMarketPrice(itemName, true, false);
		int totalPrice = itemInfo.getPrice()*itemNum;
		int saleTax = (int) (totalPrice*ItemManager.getSaleTaxRatio(itemInfo.getPrice()));
		int dailySaleTax = (int) (totalPrice*ItemManager.getDailyTaxRatio(totalPrice));
		int earn = totalPrice - saleTax - dailySaleTax;
		int result = earn / partyNum * (partyNum-1);

		out.println("<table class=\"table\" style=\"max-width:500px\"><colgroup><col width=\"50%\"><col width=\"50%\"></colgroup><thead><tr><th style=\"text-align: center;\">항목</th><th style=\"text-align: center;\">금액(개당)</th></tr></thead><tbody>");
		out.println("<tr><td>시장가(개당)</td><td style=\"text-align: center;\">"
				+ ItemManager.getPriceString(itemInfo.getPrice())
				+ "</td></tr>");
		out.println("<tr><td>총 판매가</td><td style=\"text-align: center;\">"
				+ ItemManager.getPriceString(totalPrice)
				+ "</td></tr>");
		out.println("<tr><td>판매 수수료(" +ItemManager.getSaleTaxRatio(itemInfo.getPrice())*100 + "%)</td><td style=\"text-align: center;\">"
				+ ItemManager.getPriceString(saleTax)
				+ "</td></tr>");
		out.println("<tr><td>일거래 수수료(" +ItemManager.getDailyTaxRatio(totalPrice)*100 + "%)</td><td style=\"text-align: center;\">"
				+ ItemManager.getPriceString(dailySaleTax)
				+ "</td></tr>");
		out.println("<tr class=\"success\"><td>최대입찰가</td><td style=\"text-align: center;\">"
				+  ItemManager.getPriceString(result)
				+ "</td></tr>");
		out.println("</tbody></table>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
