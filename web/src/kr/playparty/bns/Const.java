package kr.playparty.bns;


public class Const {
	public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	public static final String DB_PASSWORD = "bat800813";
	public static final String DB_ACCOUNT = "root";

	private static final String DB_URL = "jdbc:mysql://115.71.237.205:3306/bns";
	private static final String DB_URL_SANDBOX = "jdbc:mysql://115.71.237.205/bns_sandbox";

	private static boolean SANDBOX_MODE = false;
	
	public static String getDBURL()
	{
		if( SANDBOX_MODE == true )
		{
			return DB_URL_SANDBOX;
		}
		else
		{
			return DB_URL;
		}
	}

}
