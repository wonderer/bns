package kr.playparty.bns.item;

public class MaterialItemInfo {
	private String itemName;
	private int itemNum;
	private int price;
	private long updateTime;
	private int tradeNum;
	private boolean isExactSearch = true; //완전 일치로 검색할 것인가?default는 완전 일
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public int getItemNum() {
		return itemNum;
	}
	public void setItemNum(int itemNum) {
		this.itemNum = itemNum;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public void setUpdateTime(long currentTimeMillis) {
		this.updateTime = currentTimeMillis; 
		
	}
	public long getUpdateTime() {
		return updateTime;
	}
	public void setTradeNum(int tradeNum) {
		this.tradeNum = tradeNum;
	}
	public int getTradeNum() {
		return tradeNum;
		
	}
	public boolean isExactSearch() {
		return isExactSearch;
	}
	public void setExactSearch(boolean isExactSearch) {
		this.isExactSearch = isExactSearch;
	}
}
