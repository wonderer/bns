package kr.playparty.bns.item;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

public class MadeItemInfo {

	private static HashMap<String, ArrayList<MadeItemInfo>> hash= new HashMap<String, ArrayList<MadeItemInfo>>();
	private String itemName;
	private int itemNum;
	
	private int materialPrice;			//재료비
	private int madeItemPrice;			//판매가
	private int tradeNum;				//등록수
	

	private ArrayList<MaterialItemInfo> materialItemList = new ArrayList<MaterialItemInfo>();
	private int makeCost;
	private String makeTime;

	private MadeItemInfo()
	{
	}
	public String getItemName() {
		
		return itemName;
	}
	public int getItemNum() {
		return itemNum;
	}
	public static void loadData(String path, String fileName) {
		if( hash.get(fileName) == null )
		{
			File f = new File(path+"/data/" +fileName +".csv");
			try {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(new FileInputStream(f), "UTF-8"));
				
				ArrayList<MadeItemInfo> list = parseInputData(reader);
				reader.close();
				hash.put(fileName, list);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	private static ArrayList<MadeItemInfo> parseInputData(BufferedReader reader) throws IOException {
		ArrayList<MadeItemInfo> list = new ArrayList<MadeItemInfo>();

		MadeItemInfo itemInfo = null;
		String line;
		while(true)
		{
			line = reader.readLine();
			if( line == null ) {
				list.add(itemInfo);
				break;
			}
			
			
			String[] splitLines = line.split(",");
			if( splitLines[0].equals("완성품") )
			{
				if( itemInfo != null )
				{
					list.add(itemInfo);
				}
				itemInfo = new MadeItemInfo();
				itemInfo.itemName = splitLines[1];
				itemInfo.itemNum = Integer.parseInt(splitLines[2]);
			}
			else if( splitLines[0].equals("제작비"))
			{
				itemInfo.makeCost = Integer.parseInt(splitLines[2]);
			}
			else
			{
				MaterialItemInfo materialItemInfo = new MaterialItemInfo();
				materialItemInfo.setItemName(splitLines[1]);
				materialItemInfo.setItemNum(Integer.parseInt(splitLines[2]));
				itemInfo.getMaterialItemList().add(materialItemInfo);
			}
		}
		return list;
	}
	public ArrayList<MaterialItemInfo> getMaterialItemList() {
		return materialItemList;
	}
	public void setMaterialItemList(ArrayList<MaterialItemInfo> materialItemList) {
		this.materialItemList = materialItemList;
	}
	public int getMaterialPrice() {
		return materialPrice;
	}
	public void setMaterialPrice(int materialPrice) {
		this.materialPrice = materialPrice;
	}
	public int getMadeItemPrice() {
		return madeItemPrice;
	}
	public void setMadeItemPrice(int madeItemPrice) {
		this.madeItemPrice = madeItemPrice;
	}
	public int getTotalMadeItemPrice() {
		return getMadeItemPrice()*itemNum;
	}
	
	public int getGain() {
		return getTotalMadeItemPrice() - getMaterialPrice() - getSaleTaxPrice() - getDailyTaxPrice()- getMakeCost();
	}

	public int getDailyTaxPrice() {
		return (int)((float)getTotalMadeItemPrice() * getDailyTaxRatio());
	}
	public float getDailyTaxRatio() {
		return ItemManager.getDailyTaxRatio(getTotalMadeItemPrice());
	}
	public int getMakeCost() {
		return makeCost;
	}
	public static ArrayList<MadeItemInfo> getDataList(String fileName) {
		return hash.get(fileName);
	}

	public int getTradeNum() {
		return tradeNum;
	}
	public void setTradeNum(int tradeNum) {
		this.tradeNum = tradeNum;
	}
	public float getSaleTaxRatio() {
		return ItemManager.getSaleTaxRatio(getMadeItemPrice());
	}
	public int getSaleTaxPrice() {
		return (int)((float)getTotalMadeItemPrice() * getSaleTaxRatio());
	}

	static public class GainCompare implements Comparator<MadeItemInfo> {
		 
		@Override
		public int compare(MadeItemInfo o1, MadeItemInfo o2) {
			return o1.getGain() > o2.getGain() ? -1 : o1.getGain() < o2.getGain() ? 1:0;
		}
 
	}

	public static ArrayList<MadeItemInfo> getDataList(JSONArray favorite, String fileName) {
		ArrayList<MadeItemInfo> dataList = hash.get(fileName);
		ArrayList<MadeItemInfo> favoriteList = new ArrayList<MadeItemInfo>();
		
		for (MadeItemInfo madeItemInfo : dataList) {
			for( int i = 0; favorite != null && i < favorite.length(); i++ )
			{
				JSONObject object = favorite.getJSONObject(i);
				if( object.get("name").equals(madeItemInfo.getItemName()) )
				{
					favoriteList.add(madeItemInfo);
					break;
				}
			}
		}
		return favoriteList;
	}
	public String getMakeTime() {
		return makeTime;
	}
	public void setMakeTime(String time) {
		this.makeTime = time;
	}

 
}
