package kr.playparty.bns.item;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;

import kr.playparty.bns.Make;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ItemManager {

	private static HashMap<String, MaterialItemInfo> materialInfoList = new HashMap<String, MaterialItemInfo>();
//	private static int total = 0;
//	private static int search = 0;

	public static MaterialItemInfo searchMarketPrice(String itemName, boolean isExactSearch, boolean isAverage) 
				throws IOException {
		MaterialItemInfo itemInfo = new MaterialItemInfo();
		itemInfo.setItemName(itemName);
		String exactSearchOption = "";
		if( isExactSearch )
		{
			exactSearchOption = "1";
		}
		Document doc;
		doc = Jsoup
				.connect(
						"http://m.bns.plaync.com/bs/market/search?ct=&level=&stepper=&exact="+ exactSearchOption + "&sort=&type=&grade=&prevq=&q="
								+  URLEncoder.encode(itemName, "UTF-8")).timeout(60000).userAgent("Mozilla")
				.get();
		Elements eles = doc.getElementsByTag("tr");

//		if (eles.size() == 0) { //검색이 안되면 완전일치 옵션을 제거
//			doc = Jsoup
//					.connect(
//							"http://m.bns.plaync.com/bs/market/search?ct=&level=&stepper=&exact=&sort=&type=&grade=&prevq=&q="
//									+ itemName).timeout(60000).userAgent("Mozilla")
//					.get();
//			eles = doc.getElementsByTag("tr");
//		} 
		
		if (eles.size() > 0) {
			itemInfo.setTradeNum(Integer.parseInt(doc.select(".result strong").get(1).text()));

			if( isAverage ) {
				int totalValue = 0;
				int totalNum = 0;
				int firstItemPrice = 0;
				
				for( int i = 0; i < eles.size(); i++ )
				{
					Element e = eles.get(i);
					if( e.select(".bidder").text().equals("즉시거래") != true )
					{
						continue;
					}
					if( firstItemPrice == 0 )
					{
						firstItemPrice = parsePrice(e);
					}
					int itemNum = getItemNum(e);
					int priceValue = parsePrice(e);
					totalNum += itemNum;
					totalValue += (priceValue * itemNum);
				}
				int avgPrice = totalValue / totalNum;
				// 거래량이 적고 평균가가 첫번째 아이템보다 10배 차이나면 첫번째 아이템가격으로 넣는다
				if (itemInfo.getTradeNum() < Make.WARNING_TRADE_NUM && avgPrice > firstItemPrice * 10)
				{
					itemInfo.setPrice(firstItemPrice);
				} else {
					itemInfo.setPrice(avgPrice);
				}
			}
			else
			{
				itemInfo.setPrice(parsePrice(eles.get(0)));
			}
		}
		return itemInfo;
	}
	private static int getItemNum(Element e) {
		int retVal = 0;
		Elements num = e.select(".num");
		if (num.size() > 0) {
			retVal = num.size();
		}
		else
		{
			retVal = 1;
		}
	return retVal;
}
	private static int parsePrice(Element e) {
		Elements num = e.select(".num");
		Element e2 = null;
		if (num.size() > 0) {
			e2 = e.select(".price .unit").get(0);
		} else {
			e2 = e.select(".price .total").get(0);
	
		}
		Elements gold = e2.getElementsByClass("gold");
		Elements silver = e2.getElementsByClass("silver");
		Elements bronze = e2.getElementsByClass("bronze");
	
		String goldStr = gold.size() > 0 ? gold.get(0).ownText() : "0";
		String silverStr = silver.size() > 0 ? silver.get(0).ownText() : "0";
		String bronzeStr = bronze.size() > 0 ? bronze.get(0).ownText() : "0";
	
		int goldValue = Integer.parseInt(goldStr) * 10000;
		int silverValue = Integer.parseInt(silverStr) * 100;
		int bronzeValue = Integer.parseInt(bronzeStr);
		int priceValue = goldValue + silverValue + bronzeValue;
		return priceValue;
	}
	public static String getPriceString(int price) {
		int avgGold = price / 10000;
		int avgSilver = price % 10000 / 100;
		int avgBronze = price % 100;

		String priceStr = avgGold + "금" + avgSilver + "은" + avgBronze + "동";
		return priceStr;
	}
	public static MaterialItemInfo getItemInfo(String itemName, boolean isExactSearch, boolean isAverage)
			throws IOException {
//		total++;

		MaterialItemInfo itemInfo = null;
		if (itemName.equals("해적 문양 장식")) {
			itemInfo = new MaterialItemInfo();
			itemInfo.setItemName(itemName);
			itemInfo.setPrice(500000);
		}
		else if (itemName.equals("맹독 모래")) {
			itemInfo = new MaterialItemInfo();
			itemInfo.setItemName(itemName);
			itemInfo.setPrice(1000000);
		}
		else if (itemName.equals("기신귀걸이")) {
			itemInfo = new MaterialItemInfo();
			itemInfo.setItemName(itemName);
			itemInfo.setPrice(20000000);
		}
		else {
			itemInfo = materialInfoList.get(itemName);
			if (itemInfo == null) {
				itemInfo = searchMarketPrice(itemName, isExactSearch, isAverage);
				itemInfo.setUpdateTime(System.currentTimeMillis());
				materialInfoList.put(itemInfo.getItemName(), itemInfo);
//				search++;

			} else {
				//업데이트 한 지 5분이상 경과되었으면 재검색
				if (itemInfo.getPrice() == 0
						|| System.currentTimeMillis()
								- itemInfo.getUpdateTime() > 1000 * 60 * 5) 
				{
					MaterialItemInfo newSearchItemInfo = searchMarketPrice(itemName, isExactSearch, isAverage);
					itemInfo.setPrice(newSearchItemInfo.getPrice());
					itemInfo.setUpdateTime(System.currentTimeMillis());
//					search++;

				}
			}
		}
		return itemInfo;
	}
	
	public static float getSaleTaxRatio(int price) {
		
		int goldPrice = price/10000;
		float chargeRatio = 0;
		if( goldPrice < 100 )
		{
			chargeRatio = 0.05f;
		}
		else if( goldPrice < 1000 )
		{
			chargeRatio = 0.06f;
		}
		else if( goldPrice < 10000 )
		{
			chargeRatio = 0.07f;
		}
		else
		{
			chargeRatio = 0.08f;
		}
		return chargeRatio;
	}
	public static float getDailyTaxRatio(int price) {

		int goldPrice = price/10000;
		float chargeRatio = 0;
		if( goldPrice < 1000 )
		{
			chargeRatio = 0.f;
		}
		else if( goldPrice < 10000 )
		{
			chargeRatio = 0.01f;
		}
		else if( goldPrice < 50000 )
		{
			chargeRatio = 0.02f;
		}
		else
		{
			chargeRatio = 0.03f;
		}
		return chargeRatio;
	}
}

