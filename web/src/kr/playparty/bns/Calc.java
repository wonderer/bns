package kr.playparty.bns;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.playparty.bns.item.ItemManager;
import kr.playparty.bns.item.MaterialItemInfo;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Servlet implementation class Calc
 */
public class Calc extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Calc() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();

		String items = request.getParameter("ITEMLIST");
	//	items = new String(items.getBytes("8859_1"),"utf-8");
		JSONObject itemGroup = new JSONObject(items);
		String groupName = itemGroup.getString("groupname");
		JSONArray itemList = itemGroup.getJSONArray("itemlist");
		int amountPrice = 0;

		out.println("<h4>"+groupName+"</h4>");

		out.println("<table class=\"table\" style=\"max-width:500px\"><colgroup>"
				+ "<col width=\"40%\"><col width=\"30%\"><col width=\"30%\"></colgroup>"
				+ "<thead><tr><th style=\"text-align: center;\">항목</th>"
				+ "<th style=\"text-align: center;\">개당</th>"
				+ "<th style=\"text-align: center;\">총액</th></tr></thead><tbody>");

		for( int i=0; i < itemList.length(); i++)
		{
			JSONObject item = itemList.getJSONObject(i);
			String itemName = item.getString("name");
			int itemNum = item.getInt("num");
			
			MaterialItemInfo itemInfo = ItemManager.searchMarketPrice(itemName, true, true);
			int totalPrice = itemInfo.getPrice()*itemNum;
			amountPrice += totalPrice;
			out.println("<tr>"
					+ "<td>" + itemName
					+ "</td>"
					+ "<td style=\"text-align: center;\">"
					+ ItemManager.getPriceString(itemInfo.getPrice())
					+ "</td>"
					+ "<td style=\"text-align: center;\">"
					+ ItemManager.getPriceString(totalPrice)
					+ "</td>"
					+ "</tr>");

			
		}
		out.println("<tr><td colspan=2 >재료비(총합)</td><td style=\"text-align: center;\">"
				+ ItemManager.getPriceString(amountPrice)
				+ "</td></tr>");

		out.println("</tbody></table>");
		out.close();

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
