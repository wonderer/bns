/*
 * adlibr - Library for mobile AD mediation.
 * http://adlibr.com
 * Copyright (c) 2012-2013 Mocoplex, Inc.  All rights reserved.
 * Licensed under the BSD open source license.
 */

/*
 * confirmed compatible with cauly SDK 3.3.3
 */

package kr.playparty.ads;

import com.fsn.cauly.CaulyAdInfo;
import com.fsn.cauly.CaulyAdInfoBuilder;
import com.fsn.cauly.CaulyAdView;
import com.fsn.cauly.CaulyInterstitialAd;
import com.fsn.cauly.CaulyInterstitialAdListener;
import com.mocoplex.adlib.AdlibManager;
import com.mocoplex.adlib.SubAdlibAdViewCore;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;

// ?????��?? ??��?? ??��?��?? CAULY SDK �?�? 문�??�? 참조??�주??��??.
public class SubAdlibAdViewCauly extends SubAdlibAdViewCore implements com.fsn.cauly.CaulyAdViewListener  {

	protected CaulyAdView ad;
	protected boolean bGotAd = false;
	protected boolean isAdAvailable = false;
	
	// ??�기??? CAULY ID�? ?????��?��?????.
	static String caulyID = "wcYf3GkB";
	static String caulyInterstitialID = "CAULY_INTERSTITIAL_ID";
	
	public SubAdlibAdViewCauly(Context context) {
		this(context,null);
	}

	public SubAdlibAdViewCauly(Context context, AttributeSet attrs) {
		super(context, attrs);

		initCaulyView();
	}
	
	public void initCaulyView()
	{
		/* ??????�???��?? effect
		 * LeftSlide(기본) : ??�쪽?????? ??�른쪽�?��?? ??��?��?��?? 
		 * RightSlide     : ??�른쪽�????? ??�쪽??��?? ??��?��?��?? 
		 * TopSlide       : ????????? ??????�? ??��?��?��?? 
		 * BottomSlide    : ????????? ???�? ??��?��?��?? 
		 * FadeIn         : ?????? ?????? �?�?�? ????????? ??��?��????? ??�과 
		 * Circle         : ??? �???? 롤�?? 
		 * None           : ??????�???��?? ??�과 ?????? �?�? �?�? �?�?
		 */

		CaulyAdInfo ai = new CaulyAdInfoBuilder(caulyID).effect("None").bannerHeight("Proportional").build();
        
		ad = new CaulyAdView(this.getContext());
		ad.setAdInfo(ai);
		ad.setVisibility(View.GONE);
		ad.setAdViewListener(this);

		this.addView(ad);
	}
	
	@Override
	public void onReceiveAd(CaulyAdView adView, boolean isChargeableAd) {
		
		isAdAvailable = true;
		bGotAd = true;
		if(isChargeableAd)
		{
			try
			{
				if(ad != null)
				{
					ad.setVisibility(View.VISIBLE);
				}

				// ???�?�?�?�? �?????????�면 ??��?? ?????? ???면�?? ????????��?????.
				gotAd();
			}
			catch(Exception e)
			{
				failed();
			}
		}
		else
		{
			// 무�??�?�???? 보�?�주�? ?????��?????.
			failed();
		}
        
	}

	@Override
	public void onCloseLandingScreen(CaulyAdView arg0) {
	}
    
	@Override
	public void onFailedToReceiveAd(CaulyAdView arg0, int arg1, 
			String arg2) {
		
		bGotAd = true;
		failed();
	}
    
	@Override
	public void onShowLandingScreen(CaulyAdView arg0) {
	}

	// ??��??�???��???????? ????????��?? ??��????��?????.
	// ??��??�? �?�?�? 보�?�주�? ????????? ???�???��?????.
	public void query()
	{
		bGotAd = false;
		
		if(ad == null)
            initCaulyView();
		
		queryAd();
		
		ad.reload();
		
		// 3�? ??��?? 리�?��?? ?????��?? ?????�면 ??��?? ?????��?��?��?? ?????��????????.
		Handler adHandler = new Handler();
		adHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				if(bGotAd)
					return;
				else
				{
					if(ad != null)
						ad.pause();
					failed();
				}
			}
				
		}, 3000);
	}

	// �?�?뷰�?? ??��?��????? 경�?? ??��????��?????. 
	public void clearAdView()
	{
		if(ad != null)
		{
			ad.setVisibility(View.GONE);
			this.removeView(ad);
			ad.destroy();
            ad = null;
		}
		
		super.clearAdView();
	}

	// destroy ad view
	public void onDestroy()
	{
		if(ad != null)
		{
			this.removeView(ad);
			ad.destroy();
			ad = null;
		}
		
		super.onDestroy();
	}
	
	public void onResume()
	{
		if(ad != null)
		{
			// �?�? 리�?��?? ?????��?? �?�? 못�?? ???????????? ??��?��????? ????????? ??��?��??�? �?�?뷰�?? 보�?��?? ?????? ????????? 방�????��?????.
			if(!isAdAvailable)
			{
				this.removeView(ad);
				ad.destroy();
				ad = null;
				
				initCaulyView();
			}
            
			ad.resume();
		}

		super.onResume();
	}
	public void onPause()
	{
		if(ad != null)
		{
			ad.pause();
		}

		super.onPause();
	}
	
	static Handler intersHandler = null;
	static CaulyInterstitialAdListener intersListener = new CaulyInterstitialAdListener() {

		@Override
		public void onReceiveInterstitialAd(CaulyInterstitialAd ad, boolean arg1) {
			
				try
				{
	 				if(intersHandler != null)
	 				{
	 					intersHandler.sendMessage(Message.obtain(intersHandler, AdlibManager.DID_SUCCEED, "CAULY"));
	 				}
				}
				catch(Exception e)
				{
					
				}
			
			ad.show();
		}
		
		@Override
		public void onFailedToReceiveInterstitialAd(CaulyInterstitialAd ad, int errCode, String errMsg) {
			
				try
				{
	 				if(intersHandler != null)
	 				{
	 					intersHandler.sendMessage(Message.obtain(intersHandler, AdlibManager.DID_ERROR, "CAULY"));
	 				}
				}
				catch(Exception e)
				{
					
				}
		}
		
		@Override
		public void onClosedInterstitialAd(CaulyInterstitialAd ad) {
			
				try
				{
	 				// ???면�??�? ??��?????.
	 				if(intersHandler != null)
	 				{
	 					intersHandler.sendMessage(Message.obtain(intersHandler, AdlibManager.INTERSTITIAL_CLOSED, "CAULY"));
	 				} 				   		 					
				}
				catch(Exception e)
				{
					
				}					
			
		}
		
		@Override
		public void onLeaveInterstitialAd(CaulyInterstitialAd ad) {
			
		}
    };
	
	public static void loadInterstitial(Context ctx, final Handler h)
	{
		// CaulyAdInfo ??????
	    CaulyAdInfo adInfo = new CaulyAdInfoBuilder(caulyInterstitialID).build();
	    // ???�? �?�? ??????
	    CaulyInterstitialAd interstial = new CaulyInterstitialAd();
	    interstial.setAdInfo(adInfo);
	    intersHandler = h;
	    interstial.setInterstialAdListener(intersListener);
	    
	    // �?�? ???�?. �?�? ??��????? CaulyInterstitialAdListener??? onReceiveInterstitialAd?????? �?리�?????.
	    interstial.requestInterstitialAd((Activity)ctx); // ???�? �?�? ??��?? ??????�? ?????��??
	}
}