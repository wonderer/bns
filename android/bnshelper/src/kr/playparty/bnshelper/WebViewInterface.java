package kr.playparty.bnshelper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Toast;

public class WebViewInterface {
	private static final String MY_PREFS_NAME = null;
	private WebView mAppView;
    private Activity mContext;
 
    /**
     * ������.
     * @param activity : context
     * @param view : ����� ����
     */
    public WebViewInterface(Activity activity, WebView view) {
        mAppView = view;
        mContext = activity;
    }
    @JavascriptInterface
    public String loadItems (String message) {
    	SharedPreferences prefs = mContext.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE); 
    	String name = prefs.getString("cookie", "");//"No name defined" is the default value.
    	if( name.equals(""))
    	{
    		Toast.makeText(mContext, "����Ѱ� ���ٹ� �Ф�", Toast.LENGTH_SHORT).show();
    	}
    	else
    	{
    		Toast.makeText(mContext, "���� �������ٹ� ^��^", Toast.LENGTH_SHORT).show();
    	}
    	
    	return name;
    }
    @JavascriptInterface
    public void saveItems (String message) { // Show toast for a short time
    	SharedPreferences.Editor editor = mContext.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
    	editor.putString("cookie", message);
    	editor.commit();
        Toast.makeText(mContext, "���� ����ߴٹ� '��'b", Toast.LENGTH_SHORT).show();
    }

}
