package kr.playparty.bnshelper;

import com.mocoplex.adlib.AdlibAdViewContainer;
import com.mocoplex.adlib.AdlibConfig;
import com.mocoplex.adlib.AdlibManager;
import com.mocoplex.adlib.AdlibManager.AdlibVersionCheckingListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends Activity {

	// 일반 Activity 에서의 adlib 연동	
	private AdlibManager _amanager;
	private ProgressBar progressBar;
	private WebViewInterface webViewInterface;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		

		_amanager = new AdlibManager();
		_amanager.onCreate(this);
		
		AdlibConfig.getInstance().bindPlatform("CAULY","kr.playparty.ads.SubAdlibAdViewCauly");

		AdlibConfig.getInstance().setAdlibKey("540005f6e4b021b6202fed56");
		
		setContentView(R.layout.activity_main);
		this.setAdsContainer(R.id.ads);
		progressBar = (ProgressBar)findViewById(R.id.progressBar1);
		WebView webview = (WebView)findViewById(R.id.webView1);
		webview.setWebViewClient(new WebViewClientClass());
		webViewInterface = new WebViewInterface(MainActivity.this, webview); //JavascriptInterface 객체화
		webview.addJavascriptInterface(webViewInterface, "Native"); //웹뷰에 JavascriptInterface를 연결

		webview.setWebChromeClient(new WebChromeClient() {
			
			@Override
			public void onProgressChanged(WebView view, int newProgress) {
				super.onProgressChanged(view, newProgress);
				progressBar.setProgress(newProgress);
			}
			
		});
        WebSettings set = webview.getSettings();
        set.setJavaScriptEnabled(true);
        set.setBuiltInZoomControls(true);
        
//        webview.loadUrl("http://bnshelper.azurewebsites.net/bns/bns.jsp");
        webview.loadUrl("http://cosmicflight.kr/bns/bns.jsp");
	}

	  private class WebViewClientClass extends WebViewClient { //새로운 창 실행금지 웹뷰안에서 로딩.
	         @Override
	         public boolean shouldOverrideUrlLoading(WebView view, String url) {
	        	 if( url.equals("http://sandbox.plaync.com/5B3C93F8-D15F-E011-9A06-E61F135E992F") )
	        	 {
		        	 view.getContext().startActivity(
		                     new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
	        	 }
	        	 else
	        	 {
	        		 view.loadUrl(url);
	        	 }

	             return true;
	         }
	         
	         @Override
	         public void onPageStarted(WebView view, String url, Bitmap favicon) {
	        	 super.onPageStarted(view, url, favicon);
	        	 progressBar.setVisibility(View.VISIBLE);
	         }

			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				progressBar.setVisibility(View.INVISIBLE);
			}

			@Override
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				super.onReceivedError(view, errorCode, description, failingUrl);
			}
	         
	        
	  }
	  
	  @Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		  
		  
		  switch (keyCode) {
		  //하드웨어 뒤로가기 버튼에 따른 이벤트 설정
		  case KeyEvent.KEYCODE_BACK:
//			  Toast.makeText(this, "뒤로가기버튼 눌림", Toast.LENGTH_SHORT).show();
			  new AlertDialog.Builder(this)
			  .setTitle("블소도우미냥 종료")
			  .setMessage("주인님 들어가시냐묘?")
			  .setPositiveButton("응", new DialogInterface.OnClickListener() {
					  @Override
					  public void onClick(DialogInterface dialog, int which) {
					  // 프로세스 종료.
					  android.os.Process.killProcess(android.os.Process.myPid());
					  }
				  })
			  .setNegativeButton("아니", null)
			  .show();
		  break;
		  default:
		  break;
		  }
		return super.onKeyDown(keyCode, event);
	}

	protected void onResume()
		{		
			_amanager.onResume(this);
			super.onResume();
		}
		
		protected void onPause()
		{    	
			_amanager.onPause();
			super.onPause();
		}
	    
		protected void onDestroy()
		{    	
			_amanager.onDestroy(this);
			super.onDestroy();
		}

		// xml 에 지정된 ID 값을 이용하여 BIND 하는 경우
		public void setAdsContainer(int rid)
		{
			_amanager.setAdsContainer(rid);
		}
		
		// 동적으로 Container 를 생성하여, 그 객체를 통하여 BIND 하는 경우
		public void bindAdsContainer(AdlibAdViewContainer a)
		{
			_amanager.bindAdsContainer(a);		
		}
		
		// 전면광고 호출
		public void loadInterstitialAd()
		{
			_amanager.loadInterstitialAd(this);
		}
				
		// 전면광고 호출 (광고 수신 성공, 실패 여부를 받고 싶을 때 handler 이용)
		public void loadInterstitialAd(Handler h)
		{
			_amanager.loadInterstitialAd(this, h);
		}
		
		// Full Size 전면광고 호출
		public void loadFullInterstitialAd()
		{
			_amanager.loadFullInterstitialAd(this);
		}
			
		// Full Size 전면광고 호출 (광고 수신 성공, 실패 여부를 받고 싶을 때 handler 이용)
		public void loadFullInterstitialAd(Handler h)
		{
			_amanager.loadFullInterstitialAd(this, h);
		}
		
		// 팝 배너 프레임 컬러 설
		public void setAdlibPopFrameColor(int color)
		{
			_amanager.setAdlibPopFrameColor(color);
		}
		
		// 팝 배너 버튼 컬러 설정 (AdlibPop.BTN_WHITE, AdlibPop.BTN_BLACK)
		public void setAdlibPopCloseButtonStyle(int style)
		{
			_amanager.setAdlibPopCloseButtonStyle(style);
		}
		
		// 팝 배너 in, out 애니메이션 설정(AdlibPop.ANIMATION_SLIDE, AdlibPop.ANIMATION_NONE)
		public void setAdlibPopAnimationType(int inAnim, int outAnim)
		{
			_amanager.setAdlibPopAnimationType(inAnim, outAnim);
		}
		
		// 팝 배너 보이기 (align   : AdlibPop.ALIGN_LEFT, AdlibPop.ALIGN_TOP, AdlibPop.ALIGN_RIGHT, AdlibPop.ALIGN_BOTTOM)
		//             (padding : dp값)
		public void showAdlibPop(int align, int padding)
		{
			_amanager.showAdlibPop(this, align, padding);
		}
		
		// 팝 배너 숨기기
		public void hideAdlibPop()
		{
			_amanager.hideAdlibPop();
		}
		
		public void setVersionCheckingListner(AdlibVersionCheckingListener l)
		{
			_amanager.setVersionCheckingListner(l);		
		}
		
		// AD 영역을 동적으로 삭제할때 호출하는 메소드
		public void destroyAdsContainer()
		{
			_amanager.destroyAdsContainer();
		}
		// 애드립 연동에 필요한 구현부 끝    
}
