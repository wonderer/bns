//
//  ViewController.m
//  bnshelper
//
//  Created by JeongHun Kim on 2014. 9. 1..
//  Copyright (c) 2014년 ___FULLUSERNAME___. All rights reserved.
//

#import "ViewController.h"
#import <Adlib/Adlib.h>

@interface ViewController()<UIWebViewDelegate>

@end

@implementation ViewController

@synthesize webview;
- (void)viewDidLoad
{
    [super viewDidLoad];
    CGRect rt = self.view.frame;
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7 ) {
        rt.origin.y = 20;
        rt.size.height =  rt.size.height-20;
    }
    rt.size.height =  rt.size.height-50;
    self.webview = [[UIWebView alloc]initWithFrame:rt];
    self.webview.delegate = self;
    NSString *url=@"http://cosmicflight.kr/bns/bns.jsp";
    NSURL *nsurl=[NSURL URLWithString:url];
    NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
    [webview loadRequest:nsrequest];
    [self.view addSubview:webview];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
// 광고를 수신했다. 광고view 의 크기와 위치를 재설정한다.
- (void)layoutForAD
{
    CGPoint adPoint = CGPointMake(0, 0);
    CGRect rt = self.view.frame;
    
    // 광고 view의 크기
    CGSize sz = [[AdlibManager sharedSingletonClass] size];
    NSLog(@"layout : %@",NSStringFromCGSize(sz));
    
    BOOL bBannerBottom = YES;
    
    if(!bBannerBottom)
    {
        // 상단에 광고 위치
        rt = CGRectMake(0, sz.height, rt.size.width, self.view.bounds.size.height-sz.height);
    }
    else
    {
        // 하단에 광고 위치
        rt = CGRectMake(0, 0, rt.size.width, self.view.bounds.size.height-sz.height);
        adPoint = CGPointMake(0, self.view.bounds.size.height-sz.height);
    }
    
    [[AdlibManager sharedSingletonClass] moveAdContainer:adPoint];
}


- (void)viewDidAppear:(BOOL)animated
{
    [[AdlibManager sharedSingletonClass] attach:self
                                       withView:self.view
                                   withReceiver:@selector(layoutForAD)
                                 useHouseBanner:YES];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [[AdlibManager sharedSingletonClass] detach:self];
}
- (void)viewWillAppear:(BOOL)animated
{
    
}
#pragma uiwebview delegate

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    NSString *urlString = [inRequest.URL absoluteString];
    if ( [urlString hasPrefix:@"bnshelper://calc/saveitems="] ) {
        NSRange range = [urlString rangeOfString:@"bnshelper://calc/saveitems="];
        NSString *data = [urlString substringFromIndex:range.length];
        NSLog(@"save data : %@",data);
        [[NSUserDefaults standardUserDefaults] setValue :data forKey:@"items"];
    }
    if ( [urlString hasPrefix:@"bnshelper://calc/loaditems="] ) {
        NSString *items = [[NSUserDefaults standardUserDefaults] stringForKey:@"items"];
        NSString *selectedValue = [webview stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"reloadItems('%@');", items]];
        NSLog(@"load data : %@",items);
    }
    if ( ![urlString hasPrefix:@"http://cosmicflight.kr"] ) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    
    return YES;
}
@end
