//
//  AdlibPopBanner.h
//  adlibrTestUniversal
//
//  Created by Hana Kim on 2014. 1. 3..
//
//

#import <UIKit/UIKit.h>

typedef enum {
    AdlibPopBtnStyleWhite,
    AdlibPopBtnStyleBlack
} AdlibPopBtnStyle;

typedef enum {
    AdlibPopAlignLeft,
    AdlibPopAlignTop,
    AdlibPopAlignRight,
    AdlibPopAlignBottom
} AdlibPopAlign;

typedef enum {
    AdlibPopAnimationTypeNone,
    AdlibPopAnimationTypeSlide
} AdlibPopAnimationType;