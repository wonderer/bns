//
//  ViewController.h
//  bnshelper
//
//  Created by JeongHun Kim on 2014. 9. 1..
//  Copyright (c) 2014년 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property(nonatomic, retain) UIWebView* webview;
@end
